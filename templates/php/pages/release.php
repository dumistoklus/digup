<?
$is_edit = false;
?>
<div class="b-content" data-module-name="AddRelease">
	<div class="b-page__inner b-add-item__header">
		<h1 class="b-add-item__title">Добавить релиз</h1>
		<a href="add" class="b-btn is-blue is-full-colored b-add-item__add-realise">Добавить товар</a>
		<p>Прежде, чем добавить будущий товар, убедитесь, что его еще нет в Коллекции. Чтобы добавить релиз, заполните все необходимые поля. Вводите только проверенную информацию, чтобы произведения скорее попали в Коллекцию.</p>
		<p>Если вы хотели добавить товар, нажмите кнопку справа.</p>
		<a href="faq" class="b-add-item__header__link">Остались вопросы?</a>
	</div>
	<form action="" method="post">
		<div class="b-release__container">
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Исполнитель</div>
					<div class="b-form-field">
						<input type="hidden" name="artist" value="">
						<input type="text" name="artist" placeholder="Имя исполнителя или группы">
					</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Название</div>
					<div class="b-form-field">
						<input type="hidden" name="album">
						<input type="text" name="album" placeholder="Название альбома или композиции">
					</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Лейбл</div>
					<div class="b-form-field">
						<input type="hidden" name="label">
						<input type="text" name="label" placeholder="Выберите лейбл">
					</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Страна</div>
					<div class="b-form-field">
						<input type="hidden" name="country">
						<input type="text" name="country" placeholder="Страна выпуска">
					</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Дата релиза</div>
					<div class="b-form-field">
						<div class="b-form__date__day">
							<input type="text" name="day" placeholder="день">
						</div>
						<div class="b-form__date__month">
							<select name="month">
								<option value="1">Январь</option>
								<option value="2">Февраль</option>
								<option value="3">Март</option>
								<option value="4">Апрель</option>
								<option value="5">Май</option>
								<option value="6">Июнь</option>
								<option value="7">Июль</option>
								<option value="8">Август</option>
								<option value="9">Сентябрь</option>
								<option value="10">Октябрь</option>
								<option value="11">Ноябрь</option>
								<option value="12">Декабрь</option>
							</select>
						</div>
						<div class="b-form__date__year">
							<select name="year">
								<?
								for ( $i = date('Y'); $i > date('Y') - 100; $i-- ) {
									?><option value="<?= $i ?>"><?= $i ?></option><?
								}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Жанр и стиль</div>
					<div class="b-form-field">
						<select name="genre">
							<option value="0">Выберите категорию</option>
							<option value="1">Блюз</option>
							<option value="2">Джаз</option>
						</select>
					</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Обложка</div>
					<div class="b-settings__avatar">
						<div class="b-settings__avatar__text">Загрузить</div>
						<div class="b-settings__avatar__loader"></div>
						<img src="" class="b-settings__avatar__image" data-cover-image />
						<input type="file" name="cover" class="b-settings__avatar__input" data-upload-file-url="coverLoaded.php" />
					</div>
					<div class="b-settings__avatar__description">Вы можете найти изображение в интернете, сделать фотографию или сканировать обложку.</div>
				</div>
			</div>
			<div class="b-release">
				<div class="b-release__inner">
					<div class="b-release__title">Комментарий</div>
					<div class="b-form-field">
						<textarea name="comment" class="b-release__comment"></textarea>
					</div>
				</div>
			</div>
			<div class="b-release__tracks">
				<div class="b-page__inner">
					<div class="b-release__title">Трэклист</div>
					<div class="b-release__tracks__list" data-tracks-list>
						<div class="b-release__track">
							<div class="b-release__track__num"><span class="b-release__track__num__text">1</span><a href="javascript:void(0)" class="b-release__track__add" data-add-track><i class="icon-plus"></i></a></div>
							<div class="b-release__track__artist">
								<div class="b-form-field">
									<input type="hidden" name="artist[1]" value="">
									<input type="text" name="artist[1]" placeholder="Введите исполнителя" data-artist-selector>
								</div>
							</div>
							<div class="b-release__track__dash">—</div>
							<div class="b-release__track__album">
								<div class="b-form-field">
									<input type="text" name="album[1]" placeholder="Название альбома или композиции">
								</div>
							</div>
							<div class="b-release__track__minutes">
								<div class="b-form-field">
									<input type="text" name="minutes[1]">
								</div>
							</div>
							<div class="b-release__track__dots">:</div>
							<div class="b-release__track__seconds">
								<div class="b-form-field">
									<input type="text" name="seconds[1]">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="b-release__footer">
			<div class="b-page__inner">
				<div class="b-release__footer__buttons">
					<button name="draft" value="1" class="b-btn is-blue is-transparent">Сохранить в черновики</button> &nbsp; &nbsp;
					<button type="submit" class="is-blue b-btn is-full-colored"> &nbsp; &nbsp; Отправить &nbsp; &nbsp; </button>
				</div>
				<div class="b-release__footer__text">
					После нажатия на кнопку “Отправить”, ваш релиз попадет администратору на рассмотрение и после добавится в Коллекцию.
					Как только релиз будет одобрен, мы уведомим вас по почте.
				</div>
			</div>
		</div>
	</form>
	<script type="text/template" data-track-template>
		<div class="b-release__track">
			<div class="b-release__track__num"><span class="b-release__track__num__text">%num%</span><a href="javascript:void(0)" class="b-release__track__add" data-add-track><i class="icon-plus"></i></a></div>
			<div class="b-release__track__artist">
				<div class="b-form-field">
					<input type="hidden" name="artist[%num%]" value="">
					<input type="text" name="artist[%num%]" placeholder="Введите исполнителя" data-artist-selector>
				</div>
			</div>
			<div class="b-release__track__dash">—</div>
			<div class="b-release__track__album">
				<div class="b-form-field">
					<input type="text" name="album[%num%]" placeholder="Название альбома или композиции">
				</div>
			</div>
			<div class="b-release__track__minutes">
				<div class="b-form-field">
					<input type="text" name="minutes[%num%]">
				</div>
			</div>
			<div class="b-release__track__dots">:</div>
			<div class="b-release__track__seconds">
				<div class="b-form-field">
					<input type="text" name="seconds[%num%]">
				</div>
			</div>
		</div>
	</script>
</div>