'use strict';

/**
 * @class	app.views.Messages
 * @extends app.views.Default
 */
app.views.Messages = app.views.Default.extend({

	messagesUrl : 'messages.php',


	events: {
		'click [data-open-actions]'	: '_e_openActions',
		'click [data-dialog-id]'	: '_e_openDialog',
		'click [data-send-message]'	: '_e_sendMessage',
		'keyup'						: '_e_keyUp'
	},


	initialize: function() {
		this._initResize();
		this._initScroll();
	    app.views.Messages.__super__.initialize.apply(this, arguments);
		this._loadFirstDialog();
		this._$textarea = this.$('textarea');
	},


	_initResize: function() {
		this._$footer = $('.b-footer');
		this._$content = $('.b-content');
		this._$dialogs = this.$('[data-dialogs-list]');
		this._$title = this.$('[data-title]');
		this._$rightHalf = this.$('[data-right]');
		this._$dialogContainer = this.$('[data-dialog]');
		this._resize();
		this.listenTo(app, 'resize', this._resize);

		this.on('remove', function() {
			this._$footer.css('display', '');
			this._$content.css('padding-bottom', '');
			this._$footer = null;
			this._$content = null;
		});
	},


	_loadDialog: function( dialogId ) {
		var that = this;
		$.ajax({
			url: this.messagesUrl,
			data: {
				dialog_id: dialogId
			},
			dataType: 'html',
			beforeSend: function() {
				that._showMessage('Загрузка...');
			},
			success: function( data ) {
				that._destroyDialogScroll();
				that._$rightHalf.html(data);
				that._$dialogContainer = that._$rightHalf.find('[data-dialog]');
				that._$messages = that._$rightHalf.find('[data-messages]');
				that._$dialogContainer.css('height', that._dialogInnerHeight - that._newMessageHeight);
				that._$dialogContainer.scrollTop(100000);
				that._initDialogScroll();
			}
		});
	},


	_loadFirstDialog: function() {
		var query = app.utils.parseUrlQuery();
		var dialog_id;
		var $currentDialog;
		if ( query.dialog ) {
			dialog_id = query.dialog;
		} else {
			$currentDialog = this._$dialogs.find('.is-current[data-dialog-id]');
			if ( $currentDialog.length ) {
				dialog_id = $currentDialog.data('dialog-id');
			} else {
				dialog_id = this._$dialogs.find('[data-dialog-id]:first').data('dialog-id');
			}
		}
		if ( typeof dialog_id !== 'undefined' && _.isNumber(dialog_id) ) {
			this._loadDialog(dialog_id);
		} else {
			this._showMessage('Сообщений пока нет');
		}
	},


	_showMessage: function( message ) {
		this._$rightHalf.html('<div class="b-messages__warning">' + message + '</div>');
	},


	_initScroll: function() {
		var that = this;

		// Скролл для списка контактов
		this.$('[data-contacts-scroll]').each(function(){
			var $scroll = $(this);
			var childrenCount = $scroll.children().children().length;

			function getSize() {
				return {
					parentHeight: that._dialogHeight,
					innerHeight:  childrenCount * (55 + 30 + 30 + 1) // height + padding-top + padding-bottom + border-bottom
				};
			}

			$scroll.scrollAid(getSize());
			that.listenTo(app, 'resize', function() {
				$scroll.trigger('resize', getSize());
			});
		});
	},


	/**
	 * Скролл для сообщений
	 */
	_initDialogScroll: function() {
		var that				= this;
		var $dialogContainer	= this.$('[data-dialog-scroll]');
		var $inner				= $dialogContainer.find('.b-scroll__inner');

		function resize() {
			var innerHeight = $inner.outerHeight();
			$dialogContainer.trigger('resize', {
				parentHeight: that._dialogHeight - that._newMessageHeight,
				innerHeight: innerHeight
			});
			$dialogContainer.scrollTop(innerHeight);
		}

		$dialogContainer.scrollAid({
			parentHeight: that._dialogInnerHeight - that._newMessageHeight,
			innerHeight: $inner.outerHeight()
		});

		that.listenTo(app, 'resize', resize);
		that.on('addedNewMessage', resize);
	},


	_destroyDialogScroll: function() {
		this.$('[data-dialog-scroll]').trigger('destroy');
	},


	_resize: function() {
		var footerHeight = 0;
		var headerHeight = this._$content.offset().top;
		var titleHeight = this._$title.outerHeight();
		var height;

		if ( app.Size.height < 768 ) {
			this._$footer.css('display', 'none');
			this._$content.css('padding-bottom', 0);
		} else {
			this._$footer.css('display', '');
			this._$content.css('padding-bottom', '');
			footerHeight = this._$footer.outerHeight();
		}

		if ( app.Size.height > 1024 ) {
			this.$el.addClass('is-big-height');
			this._newMessageHeight = 214;
		} else {
			this.$el.removeClass('is-big-height');
			this._newMessageHeight = 214 - 45;
		}

		height = app.Size.height - footerHeight - headerHeight;
		height = Math.max(500, height);

		this._dialogHeight = height - titleHeight;
		this._dialogInnerHeight = height - titleHeight;

		this._$content.css('height', height);
		this._$dialogs.css('height', this._dialogInnerHeight);
		this._$dialogContainer.css('height', this._dialogInnerHeight - this._newMessageHeight);
	},


	_sendMessage: function( message ) {
		var you = {
			avatar: 'templates/images/_content/avatars/1.jpg',
			name: 'Виктор',
			url: 'seller'
		};
		var $message = $('<div class="b-messages__message">\
		<a href="' + you.url + '" class="b-messages__message__avatar"><img src="' + you.avatar + '" class="b-messages__message__avatar__img" /></a>\
		<a href="seller" class="b-messages__message__name">' + you.name + '</a>\
		<div class="b-messages__message__time" data-message-time>Отправляеся</div>\
		<div class="b-messages__message__text">' + message + '</div>\
		</div>');

		this._$textarea.val('');
		if ( this._$messages ) {
			this._$messages.append($message);
			this.trigger('addedNewMessage');
		}

		$.ajax({
			url: 'sendMessage.php',
			success: function( data ) {
				$message.find('[data-message-time]').html(data);
			}
		});
	},


	_e_openActions: function() {
		var that = this;
		this.$('[data-dialog-actions]').addClass('is-visible');
		app.dom.$document.on('click.dialogActions', function(){
			that.$('[data-dialog-actions]').removeClass('is-visible');
			app.dom.$document.off('click.dialogActions');
		});

		return false;
	},


	_e_openDialog: function( event ) {
		var $target = $(event.currentTarget);
		var dialogId = $target.data('dialog-id');
		$target.removeClass('is-new').addClass('is-current').siblings().removeClass('is-current');
		this._loadDialog(dialogId);
	},


	_e_sendMessage: function() {
		var message = this._$textarea.val();
		if ( message.length ) {
			this._sendMessage(message);
		}
	},


	_e_keyUp: function( event ) {
		var message = this._$textarea.val();
		if ( event.keyCode === 13 && message.length ) {
			this._sendMessage(message);
		}
	}
});