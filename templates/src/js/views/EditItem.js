'use strict';

/**
 * @class	app.views.EditItem
 * @extends app.views.AddItem
 */
app.views.EditItem = app.views.AddItem.extend({

	initialize: function() {
		app.views.EditItem.__super__.initialize.apply(this, arguments);
		this._initParams();
	},


	_initParams: function() {
		this.currentArtistId = this.$('[data-artist-info]').data('artist-info') || null;
		this.currentAlbumId = this.$('[data-album-info]').data('album-info') || null;
	}
});