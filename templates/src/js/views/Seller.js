'use strict';

/**
 * @class	app.views.Seller
 * @extends app.views.Default
 */
app.views.Seller = app.views.Default.extend({


	events: {
		'click [data-oped-wish-list]'	: '_e_openWishList',
		'click [data-oped-review-list]'	: '_e_openReviewList'
	},


	initialize: function() {
		app.views.Shop.__super__.initialize.apply(this, arguments);

		// Переносим попап на верхний уровень
		app.dom.$body.append( this.$('[data-popup]') );
	},


	_e_openWishList: function() {
		this.showPopup( $('[data-wish-list-popup]') );
		return false;
	},


	_e_openReviewList: function() {
		this.showPopup( $('[data-review-list-popup]') );
		return false;
	}
});