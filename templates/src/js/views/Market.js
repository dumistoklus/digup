'use strict';

/**
 * @class	app.views.Market
 * @extends app.views.Form
 */
app.views.Market = app.views.Form.extend({


	_itemsPerPage: 20,


	_currentPage: 0,


	/**
	 * true, если был отправлен запрос, а пользователь не дождавшись ответа запросил еще
	 * @type {boolean}
	 */
	_isWaitMore: false,


	events: {
		'click [data-add-to-cart]'	: '_addToCart',
		'click [data-view-as-list]'	: '_setItemsAsList',
		'click [data-view-as-cards]': '_setItemsAsCards',
		'change [type="checkbox"]'	: '_submitForm',
		'change [type="hidden"]'	: '_submitForm',
		'change select'				: '_submitForm',
		'click [data-more]'			: '_loadMore'
	},


	initialize: function() {
		this._$itemList = this.$('[data-items]');

		this._initListViewType();
		this._initResize();
		this._slidersInit();
		this._initFullCheckboxes();
		app.views.Market.__super__.initialize.apply(this, arguments);
		this._initPagination();
		this._url = this.$el.data('url');

	},


	_submitForm: function() {
		this._currentPage = 0;
		this._$more.css('display', '');
		this._search();
	},


	_initListViewType: function() {
		var type = app.Cookie.getCookie('listType');

		if ( type !== 'list' ) {
			type = 'card';
		}

		if ( type === 'list' ) {
			this._$itemList.addClass('is-items-as-list');
			this.$('[data-view-as-list]').addClass('is-active').next().removeClass('is-active');
		}
	},


	_loadMore: function() {
		this._currentPage++;
		this._search();
	},


	_search: function() {
		var that = this;

		if ( this._isLoad ) {
			this._isWaitMore = true;
			return;
		}
		this._isLoad = true;
		this.$el.addClass('is-loading');

		// Если была заказана первая страница - значит очищаем старый контент
		if ( !this._currentPage ) {
			this._$itemList.html('');
		}

		// ОТправляем запрос
		$.ajax({
			url: this._url + '?page=' + this._currentPage,
			dataType: 'html',
			success: complete,
			complete: function() {
				that._isLoad = false;
				that.$el.removeClass('is-loading');
			}
		});

		// Обрабаотывает ответ сервера, содержащий всю страницу
		function complete( data ) {
			if ( that._isWaitMore ) {
				that._isWaitMore = false;
				that._search();
				return;
			}

			// Выбираем участок для создания dom документа, чтобы исключить пападания туда скриптов
			// Если засунуть все это в jQuery, то все скрипты выполнятся второй раз
			var targetContent = /<!-- ITEMS -->([\S\s]*)<!-- \/ ITEMS -->/.exec( data );

			// Делаем редирект, если пришла не стандартная страница
			if ( targetContent && targetContent[1] ) {
				that._insertBlocks($(targetContent[1]));
				that._resize();
			}
		}
	},


	_insertBlocks: function( $items ) {
		this._$itemList.append($items);
		this._checkCount();
	},


	_initPagination: function() {
		this._$more = this.$('[data-more]');
		this._checkCount();
	},


	_checkCount: function() {
		var itemsCount = this._$itemList.children().length;
		if ( itemsCount % this._itemsPerPage !== 0) {
			this._$more.css('display', 'none');
		}
	},


	_initResize: function() {
		this.$menu		= this.$('.b-market__menu');
		this.$results	= this.$('.b-market__results');
		this._resize();
		this.listenTo(app, 'resize', this._resize);
	},


	_resize: function() {
		this.$menu.height('auto');
		var menuHeight		= this.$menu.height();
		var resultsHeight	= this.$results.outerHeight();

		if ( menuHeight < resultsHeight ) {
			this.$menu.height(resultsHeight);
		}
	},


	_initFullCheckboxes: function() {
		var lastOpenIndex	= null;
		var that			= this;
		var $layers			= this.$('.is-contains-full-checkboxes');

		function openFullBox( event ) {
			var $target = $(event.target);
			var $layer	= $target.closest('.is-contains-full-checkboxes');

			$layers.each(function( key ){
				if ( $(this).is( $layer ) ) {
					lastOpenIndex = key;
					$layer.addClass('is-boxes-open');
					$layer.find('[data-full-checkboxes]').fadeIn(200);
				}
			});
		}

		function closeBox() {
			var $layer = $layers.eq(lastOpenIndex);
			$layer.find('[data-full-checkboxes]').fadeOut(200);
			$layer.delay(200).removeClass('is-boxes-open');
			lastOpenIndex = null;
		}

		app.dom.$document.on('click', function( event ) {
			var $target = $(event.target);
			var $arrow	= $target.filter('[data-show-full]').length ? $target : $target.closest('[data-show-full]');
			var inOpen	= $arrow.length;
			var noOpen	= false;

			// Закрываем, если открыто
			// И если это клик не по чекбоксам
			// И был клик на стрелку и эта стрелка была от отрытой
			if ( lastOpenIndex !== null && (!$target.closest('[data-full-checkboxes]').length ) ) {
				noOpen = inOpen && $layers.eq(lastOpenIndex).find('[data-show-full]').is($arrow);
				closeBox();
			}
			if ( inOpen && !noOpen ) {
				openFullBox(event);
			}
		});

		this.$el.on('change', '[type="checkbox"]', function() {
			var $this		= $(this);
			var isChecked	= !!$this.filter(':checked').length;
			var name		= $this.attr('name');
			var inBox		= $this.closest('[data-full-checkboxes]').length;
			var $box		= inBox ? $this.closest('[data-full-checkboxes]') : $this.closest('.b-form__checkboxes').siblings('[data-full-checkboxes]');
			var $main		= $box.prev();

			// Чекнули в коробке - повторяем в коробке
			if ( inBox ) {
				$main.find('[name="' + name +'"]').each(function(){
					this.checked = isChecked;
				});

				// Если теперь она ченута - пробуем склонировать
				if ( isChecked ) {
					// Если такой еще нет - добавляем
					if ( !$main.find('[name="' + name +'"]').length ) {
						setTimeout(function(){
							$main.append($this.parent().clone());
							$main.find('[name="' + name +'"]').parent().find('.jq-checkbox').remove().end().find('[name="' + name +'"]').checkbox();
							that._resize();
						}, 40);
					}
				}
			}

			// Чекнули вне коробки - изменяем внутри
			if ( !inBox ) {
				$box.find('[name="' + name +'"]').each(function(){
					this.checked = isChecked;
				});
			}
		});
	},


	_slidersInit: function() {
		this.$('[data-range-slider]').each(function() {
			var $this			= $(this);
			var $slider			= $this.find('[data-slider]');
			var $firstInput		= $this.find('[data-slider-from]');
			var $secondInput	= $this.find('[data-slider-to]');
			var $hint			= $this.find('[data-slider-hint]');
			var hintSting		= $hint.data('string');
			var min				= $slider.data('min');
			var max				= $slider.data('max');

			function setString( val1, val2 ) {
				var str = hintSting.replace('%num%', val1);
				str = str.replace('%num%', val2);
				$hint.text(str);
			}

			$slider.slider({
				range	: true,
				min		: min,
				max		: max,
				values	: [min, max],
				slide	: function(event, ui) {
					$firstInput.val(ui.values[0]);
					$secondInput.val(ui.values[1]);
					setString(ui.values[0], ui.values[1]);
				},
				create	: function() {
					$slider.append('<div class="ui-slider-bg" />');
				},
				stop: function() {
					$secondInput.change();
				}
			});

			setString(min, max);
		});

		this.$('[data-step-slider]').each(function(){
			var $this			= $(this);
			var $slider			= $this.find('[data-slider]');
			var $input			= $this.find('[data-slider-value]');
			var value			= $input.val();
			var $corners;

			$slider.slider({
				range	: 'max',
				min		: 0,
				max		: 4,
				value	: value,
				slide	: function(event, ui) {
					$input.val(ui.value).change();
					$corners.removeClass('is-highlighted').each(function(key){
						if ( key > ui.value ) {
							$(this).addClass('is-highlighted');
						}
					});
				},
				create	: function() {
					$slider.append('<div class="ui-slider-bg" />');
					$slider.append($slider.next());
					$corners = $slider.children(':last').children();

					$corners.each(function(key){
						if ( key > value ) {
							$(this).addClass('is-highlighted');
						}
					});
				},
				stop: function() {
					$input.change();
				}
			});
		});
	},


	_setItemsAsList: function() {
		this._$itemList.addClass('is-items-as-list');
		this._setListType('list');
		this.$('[data-view-as-list]').addClass('is-active').next().removeClass('is-active');
	},


	_setItemsAsCards: function() {
		this._$itemList.removeClass('is-items-as-list');
		this._setListType('card');
		this.$('[data-view-as-list]').removeClass('is-active').next().addClass('is-active');
	},


	_setListType: function( type ) {
		var time = new Date();
		time.setYear(time.getFullYear() + 1);
		app.Cookie.setCookie('listType', type, time, '/');
	},


	_addToCart: function( event ) {
	    var $target = $(event.currentTarget);
		var id = $target.closest('[data-item-id]').data('item-id');
		location.href = 'cart?add=' + id;
		return false;
	}
});