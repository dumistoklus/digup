'use strict';

/**
 * @class	app.views.Default
 * @extends Backbone.View
 */
app.views.Default = Backbone.View.extend({

	el					: '.b-content',

	_player				: null,
	_isPlayerInitialized: false,
	_isPlayed			: false,
	_lastSrc			: null,


	/**
	 * Выбирает таб
	 * @param event
	 * @private
	 */
	_e_selectTab: function( event ) {
		var $target	= $(event.currentTarget);
		var tabNum	= $target.data('select-tab');

		if ( !$target.hasClass('is-selected') ) {
			this.$('[data-tab="' + tabNum + '"]').addClass('is-showed').siblings().removeClass('is-showed');
			$target.addClass('is-selected').siblings().removeClass('is-selected');
		}
	},


	/**
	 * Стартует радио поток
	 * @public
	 * @param [url]
	 */
	play: function( url ) {
		this.pause();
		if ( !this._isPlayerInitialized ) {
			this._initPlayerElement();
		}
		if ( url ) {
			this._setSource(url);
		}
		if ( !this._isPlayed ) {
			this.trigger('player:started');
			this._bindPlayerEvents();
			this._player.play();
			this._isPlayed = true;
		}
	},


	/**
	 * Останваливает радио поток
	 * @public
	 */
	pause: function() {
		if ( this._isPlayed ) {
			this.trigger('player:stopped');
			this._unbindPlayerEvents();
			this._player.pause();
			this._isPlayed = false;
		}
	},


	/**
	 * Устанавливает адрес трека в плеер
	 * @public
	 */
	_setSource: function( url ) {
		this._player.src	= url;
		this._lastSrc		= url;
	},


	/**
	 * Инициализирует тег аудио для проигрывания радио
	 */
	_initPlayerElement: function() {
		this._player = document.createElement('audio');
		app.dom.$body.append(this._player);
		this._isPlayerInitialized = true;
	},


	/**
	 * Навешивает на плеер события загрузки данных
	 * @private
	 */
	_bindPlayerEvents: function() {
		var that		= this,
			$player		= $(that._player)
		;

		function playerDone() {
			that.trigger('player:loadEnd');
			that._unbindPlayerEvents();
		}

		// Начало загрузки
		$player.bind('loadstart', _.once(function(){
			that.trigger('player:loadStart');
		}));

		// Начало проигрывания
		$player.bind('playing', _.once(playerDone));
	},


	/**
	 * Снимает с плеера события загрузки данных
	 * @private
	 */
	_unbindPlayerEvents: function() {
		$(this._player).unbind('loadend loadeddata loadstart');
	},


	showPopup: function( $popup ) {
		var scrollPosition	= app.utils.getScrollPosition();

		app.dom.$main.css({
			top: -scrollPosition,
			position: 'fixed'
		});
		app.dom.$window.scrollTop(0);

		$popup.fadeIn();

		$popup.on('click', function( event ){
			var $target = $(event.target);
			if ( $target.filter('[data-popup]').length ) {
				$target.fadeOut(function(){
					app.dom.$main.css({
						top: '',
						position: ''
					});
					app.dom.$window.scrollTop(scrollPosition);
				});
				if ( !$popup.hasClass('no-generated') ) {
					$popup.remove();
				}
			}
		});
	},


	/**
	 * Отскроливает к нужной позиции
	 * @public
	 * @param	{Number}	scrollPosition
	 * @param	{Function}	[callback]
	 * @param	{Number}	[duration]
	 */
	scrollTo: function( scrollPosition, callback, duration ) {
		var $htmlBody = $('html,body');

		if ( scrollPosition < 0 ) {
			scrollPosition = 0;
		}

		if ( callback ) {
			callback = _.once(callback)
		}

		if ( duration ) {
			$htmlBody.animate({
				scrollTop: scrollPosition
			}, {
				duration: duration,
				complete: callback
			});
		} else {
			$htmlBody.scrollTop(scrollPosition);
			if ( typeof callback === 'function') {
				callback();
			}
		}
	}
});