<script>
	window.form_success_messages = {
		notifications: 'Уведомления по email сохранены',
		settings: 'Данные сохранены'
	};
</script>
<div class="b-content" data-module-name="Settings">
	<div class="b-page__main-title">Настройки аккаунта</div>
	<div class="b-page__line b-page__line__tabs">
		<a href="javascript:void(0)" class="b-page__line__tab is-selected" data-select-tab="0">Аккаунт</a><!--
		--><a href="javascript:void(0)" class="b-page__line__tab" data-select-tab="1">Уведомления</a>
	</div>

	<div class="b-settings__notifications is-showed" data-tab="0">
		<form action="save.php" method="post" data-message-type="settings">
			<div class="b-page__inner">
				<div class="b-settings__half">
					<div class="b-settings__half__inner">
						<div class="b-form__title">Личные данные</div>
						<div class="b-form-field">
							<input type="text" name="first_name" placeholder="Имя" />
						</div>
						<div class="b-form-field">
							<input type="text" name="last_name" placeholder="Фамилия" />
						</div>
						<div class="b-form-field">
							<input type="text" name="nickname" placeholder="Никнейм" />
						</div>
						<div class="b-form-field">
							<input type="text" name="email" placeholder="Email" />
						</div>
					</div>
				</div>
				<div class="b-settings__half">
					<div class="b-settings__half__inner">
						<div class="b-form__title">Адрес доставки</div>
						<div class="b-form-field">
							<input type="text" name="country" placeholder="Страна" />
						</div>
						<div class="b-form-field">
							<input type="text" name="city" placeholder="Город" />
						</div>
						<div class="b-form-field">
							<input type="text" name="zip" placeholder="Индекс" />
						</div>
						<div class="b-form-field">
							<input type="text" name="address" placeholder="Улица, дом, квартира / офис" />
						</div>
					</div>
				</div>
			</div>
			<div class="b-settings__line"></div>
			<div class="b-page__inner">
				<div class="b-settings__half">
					<div class="b-settings__half__inner" data-save-password-url="save.php">
						<div class="b-form__title">Сменить пароль</div>
						<div class="b-form-field">
							<input type="password" name="old_password" placeholder="Старый пароль" data-old-password />
						</div>
						<div class="b-form-field">
							<input type="password" name="new_password" placeholder="Новый пароль" data-new-password />
						</div>
						<div class="b-form-field">
							<input type="password" name="new_password_repeat" placeholder="Повторите новый пароль" data-repeat-password />
						</div>
						<a href="javascript:void(0)" class="b-btn is-blue is-full-colored" data-change-password data-url="changePassword.php" data-message-passwords-not-match="Пароли не совпадают!" data-message-changed="Пароль изменен!" data-message-no-repeat="Пароли не совпадают"> &nbsp; &nbsp; Сменить &nbsp; &nbsp; </a>
					</div>
				</div>
				<div class="b-settings__half">
					<div class="b-settings__half__inner">
						<div class="b-form__title">Сменить аватар</div>
						<div class="b-settings__avatar">
							<div class="b-settings__avatar__text">Загрузить</div>
							<div class="b-settings__avatar__loader"></div>
							<img src="" class="b-settings__avatar__image" data-avatar-image />
							<input type="file" name="avatar" class="b-settings__avatar__input" data-upload-file-url="avatarLoaded.php" />
						</div>
						<div class="b-settings__avatar__description">Файл не должен
							превышать 2 мб
						</div>
					</div>
				</div>
			</div>
			<div class="b-settings__line"></div>
			<div class="b-page__inner">
				<div class="b-settings__half">
					<div class="b-settings__half__inner">
						<div class="b-form__title">Лицевой счет</div>
						<div class="b-form-field">
							<input type="text" name="card_number" placeholder="Номер карты" />
						</div>
						<div class="b-form-field">
							<input type="text" name="card_address" placeholder="Адрес регистрации карты" />
						</div>
						<div class="b-form-field">
							<input type="text" name="card_person" placeholder="Имя и фамилия владельца латиницей" />
						</div>
						<div class="b-form-field">
							<input type="text" name="card_date" placeholder="Срок действия карты" />
						</div>
					</div>
				</div>
				<div class="b-settings__half">
					<div class="b-settings__half__inner">
						<div class="b-form__title">Аккаунт paypal</div>
						<div class="b-form-field">
							<input type="text" name="card_number" placeholder="Номер карты" />
						</div>
						<div class="b-settings__description">Мы надежно храним информацию о каждом пользователе и не передаем ее третьим лицам.</div>
						<a href="" class="b-settings__more-link">Узнать больше</a>
					</div>
				</div>
				<div class="b-settings__buttons">
					<div class="b-settings__half">
						<div class="b-settings__half__inner">
							<button class="b-btn is-blue is-full-colored"> &nbsp; &nbsp; Сохранить &nbsp; &nbsp; </button>
						</div>
					</div>
					<div class="b-settings__half">
						<div class="b-settings__half__inner">
							<a href="" class="b-btn is-blue">Удалить аккаунт</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="b-settings__notifications" data-tab="1">
		<div class="b-page__inner">
			<div class="b-settings__title">Уведомления по email</div>
			<form action="save.php" method="post" data-message-type="notifications">
				<div class="b-settings__half b-form__checkboxes">
					<label>
						<input type="checkbox" name="settings1"> Новое личное сообщение
					</label>
					<label>
						<input type="checkbox" name="settings2"> Заявка на покупку вашего товара
					</label>
					<label>
						<input type="checkbox" name="settings3" checked> Вам выставили рейтинг
					</label>
				</div>
				<div class="b-settings__half b-form__checkboxes">
					<label>
						<input type="checkbox" name="settings4"> В вашем профиле оставили комментарий
					</label>
					<label>
						<input type="checkbox" name="settings5"> В вашем профиле оставили отзыв
					</label>
					<label>
						<input type="checkbox" name="settings6" checked> Срок времени товара в закладках истекает
					</label>
					<label>
						<input type="checkbox" name="settings7"> Еженедельные дайджесты сервиса
					</label>
				</div>
				<div class="b-settings__button">
					<button class="b-btn is-blue is-full-colored">&nbsp; &nbsp; Сохранить &nbsp; &nbsp;</button>
				</div>
			</form>
		</div>
	</div>
</div>