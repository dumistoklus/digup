module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),


		// Сжатие кода
		uglify: {
			options: {
				banner	: '/*! Do not touch this file! */\n',
				report	: 'min',
				compress: true
			},
			build: {
				src: [
					'src/js/app.js',
					'src/js/Router.js',
					'src/js/helpers/utils.js',
					'src/js/helpers/*.js',
					'src/js/views/common/DefaultView.js',
					'src/js/views/common/HeaderView.js',
					'src/js/views/common/Form.js',
					'src/js/views/*.js',
					'src/js/main.js'
				],
				dest: 'build/script.min.js'
			}
		},


		// Конкатенация файлов
		concat: {
			options: {
				banner: '/*! Do not touch this file! */\n',
				stripBanners: true,
				nonull: true
			},

			concatjs: {
				src: [
					'src/js/libs/jquery-2.1.1.min.js',
					'src/js/libs/underscore-1.6.0.min.js',
					'src/js/libs/backbone-1.1.2.min.js',
					'src/js/libs/modernizr.2.8.2.custom.min.js',
					'src/js/libs/jquery.ikSelect.min.js',
					'src/js/libs/checkbox.min.js',
					'src/js/libs/jquery-ui.min.js'
				],
				dest: 'build/base.min.js'
			}
		},

		// Стили
		less: {
			production: {
				options: {
					cleancss: false,
					compress: false,
					report: 'min'
				},
				files: {
					"build/styles.min.css": "src/less/styles.less"
				}
			}
		},
		
		autoprefixer: {
			options: {
				browsers: ['> 2%', 'last 2 versions']
			},
			single_file: {
				src: 'build/styles.min.css'
			}
		},
		
		watch: {
			css: {
				files: [
					'src/less/*.less',
					'src/less/**/*.less'
				],
				tasks: ['less', 'autoprefixer']
			},
			options: {
				livereload: true
			}
		}
	});


	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('css', ['less', 'autoprefixer']);
	grunt.registerTask('js', ['uglify', 'concat']);

	grunt.registerTask('default', ['css', 'js']);
};