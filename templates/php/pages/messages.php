<div class="b-content" data-module-name="Messages">
	<div class="b-page__inner b-messages">
		<div class="b-messages__left">
			<div class="b-messages__title" data-title>Сообщения</div>
			<div class="b-messages__dialogs b-scroll__wrapper" data-dialogs-list>
				<div class="b-scroll" data-contacts-scroll>
					<div class="b-scroll__inner">
						<a href="javascript:void(0)" class="b-messages__dialogs__el is-new" data-dialog-id="1">
							<div class="b-messages__dialogs__el__date">14.07.2013</div>
							<div class="b-messages__dialogs__el__img-wrapper">
								<img src="templates/images/_content/avatars/2.jpg" class="b-messages__dialogs__el__img" />
							</div>
							<div class="b-messages__dialogs__el__name">
								Sonic
							</div>
							<div class="b-messages__dialogs__el__message">Здрасте, хочу купить сто тыщ пластинок!! Один</div>
						</a>
						<a href="javascript:void(0)" class="b-messages__dialogs__el is-current" data-dialog-id="2">
							<div class="b-messages__dialogs__el__date">14.07.2013</div>
							<div class="b-messages__dialogs__el__img-wrapper">
								<img src="templates/images/_content/avatars/1.jpg" class="b-messages__dialogs__el__img" />
							</div>
							<div class="b-messages__dialogs__el__name">
								Sonic
							</div>
							<div class="b-messages__dialogs__el__message">ОЙ. а что это у вас в новиках появилось?</div>
						</a>
						<a href="javascript:void(0)" class="b-messages__dialogs__el" data-dialog-id="1">
							<div class="b-messages__dialogs__el__date">14.07.2013</div>
							<div class="b-messages__dialogs__el__img-wrapper">
								<img src="templates/images/_content/avatars/3.jpg" class="b-messages__dialogs__el__img" />
							</div>
							<div class="b-messages__dialogs__el__name">
								Sonic
							</div>
							<div class="b-messages__dialogs__el__message">Достал урод! Сам покупай свои пластинки!</div>
						</a>
						<a href="javascript:void(0)" class="b-messages__dialogs__el" data-dialog-id="1">
							<div class="b-messages__dialogs__el__date">14.07.2013</div>
							<div class="b-messages__dialogs__el__img-wrapper">
								<img src="templates/images/_content/avatars/2.jpg" class="b-messages__dialogs__el__img" />
							</div>
							<div class="b-messages__dialogs__el__name">
								Sonic
							</div>
							<div class="b-messages__dialogs__el__message">Здрасте, хочу купить сто тыщ пластинок!! Один</div>
						</a>
						<a href="javascript:void(0)" class="b-messages__dialogs__el" data-dialog-id="1">
							<div class="b-messages__dialogs__el__date">14.07.2013</div>
							<div class="b-messages__dialogs__el__img-wrapper">
								<img src="templates/images/_content/avatars/1.jpg" class="b-messages__dialogs__el__img" />
							</div>
							<div class="b-messages__dialogs__el__name">
								Sonic
							</div>
							<div class="b-messages__dialogs__el__message">ОЙ. а что это у вас в новиках появилось?</div>
						</a>
						<a href="javascript:void(0)" class="b-messages__dialogs__el" data-dialog-id="1">
							<div class="b-messages__dialogs__el__date">14.07.2013</div>
							<div class="b-messages__dialogs__el__img-wrapper">
								<img src="templates/images/_content/avatars/3.jpg" class="b-messages__dialogs__el__img" />
							</div>
							<div class="b-messages__dialogs__el__name">
								Sonic
							</div>
							<div class="b-messages__dialogs__el__message">Достал урод! Сам покупай свои пластинки!</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="b-messages__right">
			<div data-right></div>
			<div class="b-messages__new-message">
				<form>
					<textarea name="message" placeholder="Ваше сообщение..."></textarea>
					<input type="submit" class="is-hidden" />
					<a href="javascript:void(0)" class="b-btn is-blue is-full-colored" data-send-message>Отправить</a>
				</form>
			</div>
		</div>
	</div>
</div>