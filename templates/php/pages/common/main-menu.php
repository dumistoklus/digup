<div class="top-menu__wrapper">
	<div class="top-menu">
		<a href="./" class="top-menu__logo"><img src="templates/images/logo.svg" alt="digup.ru logo" class="is-black-logo" /><img src="templates/images/logo_white.svg" alt="digup.ru logo" class="is-white-logo" /></a>
		<div class="top-menu__links">
			<a href="stream" class="top-menu__links__link"><span class="icon-refresh"></span>Лента</a>
			<a href="collection" class="top-menu__links__link">Раскопки</a>
			<a href="faq" class="top-menu__links__link">Вопросы-ответы</a>
			<a href="" class="top-menu__links__link is-active">Блог</a>
			<a href="" class="top-menu__links__link">О проекте</a>
		</div>
	</div>
</div>