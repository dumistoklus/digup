'use strict';

(function (){

	var notificationVisibleTime = 3000;
	var messageVisibleTime = 5000;


	/**
	 * Создает DIV с уведомлением
	 * @param message
	 * @returns {*|jQuery|HTMLElement}
	 */
	function createNotification( message ) {
		var html = '<div class="b-notifications__message"><i class="icon-ok b-notifications__message__ok"></i>' +
			'<div class="b-notifications__message__text">' + message + '</div></div>';

		return appendNotice(html);
	}


	/**
	 * Создает DIV с уведомлением о сообщении
	 * @param message
	 * @returns {*|jQuery|HTMLElement}
	 */
	function createMessage( message ) {
		var html = '<a href="' + message.url + '" class="b-notifications__message"><i class="icon-message b-notifications__message__ok"></i>' +
			'<div class="b-notifications__message__text">' + message.text + ' <span class="b-notifications__message__user">' + message.user + '</span></div></a>';

		return appendNotice(html);
	}


	/**
	 * Создает DIV с уведомлением об ошибке
	 * @param message
	 * @returns {*|jQuery|HTMLElement}
	 */
	function createError( message ) {
		var html = '<div class="b-notifications__message"><i class="icon-close b-notifications__message__fail"></i>' +
			'<div class="b-notifications__message__text">' + message + '</div></div>';

		return appendNotice(html);
	}


	function initNotificationEvents( $notification ) {
		var timeout;

		$notification.click(function(){
			hideNotification($notification);
			clearTimeout(timeout);
		});

		timeout = setTimeout(function(){
			hideNotification($notification);
		}, notificationVisibleTime);
	}

	function appendNotice( html ) {
		var $notice = $(html);
		$('[data-notifications]').append($notice);
		$notice.slideDown(200);
		return $notice;
	}

	function hideNotification( $notification ) {
		$notification.slideUp(function(){
			$(this).remove();
		});
	}

	function initMessageEvents( $message ) {
		var timeout;

		$message.click(function() {
			clearTimeout(timeout);
		});

		timeout = setTimeout(function() {
			hideNotification($message);
		}, messageVisibleTime);
	}


	app.utils.showNotification = function( message ) {
		var $notification = createNotification(message);
		initNotificationEvents($notification);
	};

	app.utils.showMessage = function( message ) {
		var $message = createMessage(message);
		initMessageEvents($message);
	};

	app.utils.showError = function( errorText ) {
		var $error = createError(errorText);
		initNotificationEvents($error);
	};


	/**
	 * При инициализации сообщений должен быть создан массив window.notifications с сообщениями, которые нужно показать
	 */
	app.utils.initNotification = function() {
		if ( window.notifications && _.isArray( window.notifications ) ) {
			window.notifications.forEach(function( text ) {
				app.utils.showNotification(text);
			});
		}
		if ( window.messages && _.isArray( window.messages ) ) {
			window.messages.forEach(function( text ) {
				app.utils.showMessage(text);
			});
		}
		if ( window.errors && _.isArray( window.errors ) ) {
			window.errors.forEach(function( text ) {
				app.utils.showError(text);
			});
		}
	};


})();