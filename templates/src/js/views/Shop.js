'use strict';

/**
 * @class	app.views.Shop
 * @extends app.views.Default
 */
app.views.Shop = app.views.Default.extend({

	_lastTrackId			: 0,
	_currentPlayedTrackId	: null,

	events: {
		'click [data-open-post]' : '_e_openPost'
	},


	initialize: function() {
		app.views.Shop.__super__.initialize.apply(this, arguments);

		// Переносим попап на верхний уровень
		console.log(this.$('[data-popup]'));
		app.dom.$body.append( this.$('[data-popup]') );
	},


	_e_openPost: function() {
		this.showPopup( $('[data-popup]') );
		return false;
	}
});