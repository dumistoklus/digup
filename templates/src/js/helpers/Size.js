'use strict';

(function (){

	var _android_delay	= 300;

	function Size() {

		var __bind	= function(fn, me){ return function(){ return fn.apply(me, arguments); };},
			_this	= this,
			_events	= /iPod|iPad|iPhone/g.test(navigator.userAgent) ? 'orientationchange load' : 'resize load' // айфон или все остальные
		;

		_this.width	= null;
		_this.height = null;
		_this.orientationChange = __bind(_this.orientationChange, _this);

		// делаем задержку для андроида
		if ( /android/ig.test(navigator.userAgent) ) {
			app.dom.$window.on('orientationchange resize load', function() {
				return setTimeout(_this.orientationChange, _android_delay);
			});
		} else {
			app.dom.$window.on(_events, _this.orientationChange);
		}
		_this.orientationChange();
	}

	Size.prototype = {

		orientationChange: function() {
			var that		= this,
				prevHeight	= that.height,
				prevWidth	= that.width
			;

			that.width	= Modernizr.touch ? window.innerWidth : app.dom.$window.width();
			that.height	= Modernizr.touch ? window.innerHeight : app.dom.$window.height();

			if ( that.width !== prevWidth || that.height !== prevHeight ) {
				app.trigger('resize', that.getSize() );
			}
		},


		getSize: function() {
			return {
				width:	this.width,
				height:	this.height
			};
		}
	};

	app.Size = new Size();
})();
