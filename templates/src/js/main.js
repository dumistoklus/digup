'use strict';

(function(){
	Modernizr.transformPrefix	= Modernizr.testAllProps('transform','pfx');
	Modernizr.transitionPrefix	= Modernizr.testAllProps('transition','pfx');

	// Шапка с менюхой
	app.HeaderView = new app.views.HeaderView();

	app.utils.initNotification();

	// Выбираем вьюху
	(function(){
		var $content		= $('.b-content');
		var currentViewName = $content.data('module-name');
		if ( app.views[currentViewName] ) {
			app.CurrentView = new app.views[currentViewName]({
				el: $content
			});
		}
	})();

	// Контейнер
	(function(){
		function resize() {
			var width = Math.max(1000, app.Size.width);
			app.dom.$main.css('width', width);
		}

		app.on('resize', resize);
		resize();
	})();
})();