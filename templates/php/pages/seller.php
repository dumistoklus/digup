<div class="b-content" data-module-name="Seller">
	<div class="b-page__header">
		<div class="b-page__inner">
			<div class="b-page__header__stat-and-buttons">
				<div class="b-page__header__stat-and-buttons__stat">
					<span class="b-item__stat__rating">
						<span class="icon-stat"></span><br />4.8
					</span>
					<span class="b-item__stat__plus">
						<span class="icon-eye"></span><br />15
					</span>
					<span class="b-item__stat__ok">
						<span class="icon-ok"></span><br />332
					</span>
				</div>
				<div class="b-page__header__stat-and-buttons__buttons">
					<a href="javascript:void(0)" class="b-btn is-green is-block is-full-colored b-page__header__stat-and-buttons__buttons__button">Отправить сообщение</a>
					<a href="javascript:void(0)" class="b-btn is-green is-block b-page__header__stat-and-buttons__buttons__button"><i class="icon-eye"></i>Следить за продавцом</a>
				</div>
			</div>
			<div class="b-page__header__cover">
				<img src="templates/images/_content/avatars/big/1.jpg">
			</div>
			<div class="b-page__header__main">
				<h1 class="b-page__header__title">
					Steven Baskerville
				</h1>
				<div class="b-page__header__subtitle">
					ottawa, canada
				</div>
				<div class="b-page__params">
					<div class="b-page__params__dt">Товаров</div>
					<div class="b-page__params__dd">22</div>
					<div class="b-page__params__dt">Сделок</div>
					<div class="b-page__params__dd">28</div>
					<div class="b-page__params__dt">оплата</div>
					<div class="b-page__params__dd">paypal, яндекс.деньги, visa, mastercard</div>
				</div>
			</div>
		</div>
	</div>
	<div class="b-page__inner">
		<div class="b-page__content">
			<div class="b-page__title">Товары продавца</div>
			<?php include('common/itemList.php') ?>
			<div class="b-page__title">Похожие товары</div>
			<div class="b-item-short-list">
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/7.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/4.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Dark Side Of The Mushroom</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/5.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/2.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/4.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Dark Side Of The Mushroom</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/2.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/7.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/5.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
			</div>
		</div>
		<div class="b-page__info">
			<div class="b-page__title">О продавце</div>
			<div class="b-page__info__block">Is an American rapper who performs under the name Talib Kweli. His first name, Talib, in Arabic means "student" or "seeker"; his middle name in Swahili means "true". </div>
			<div class="b-page__info__block">
				<div class="b-page__info__link__block">
					<a href="" class="b-page__info__link"><i class="icon-arrow-right"></i> soundcloud</a>
				</div>
				<div class="b-page__info__link__block">
					<a href="" class="b-page__info__link"><i class="icon-arrow-right"></i> personal blog</a>
				</div>
			</div>
			<div class="b-page__info__block">
				<div class="b-page__info__social">
					<a href="https://twitter.com/share" class="twitter-share-button" data-related="jasoncosta" data-lang="en" data-count="horizontal" data-url="http://feldvolk.com/">Tweet</a>
				</div>
				<div class="b-page__info__social">
					<div class="fb-like" data-send="false" data-href="https://www.facebook.com/feldvolk" data-layout="button_count" data-width="150" data-show-faces="false"></div>
				</div>
			</div>
			<div class="b-page__title">Вишлист</div>
			<div class="b-page__info__wish-list">
				<a href="" class="b-page__info__wish-list__cover">
					<img src="templates/images/_content/items/1.jpg"/>
					<img src="templates/images/_content/items/2.jpg"/>
					<img src="templates/images/_content/items/3.jpg"/>
				</a>
				<a href="item" class="b-page__info__wish-list__title">Heart Of Sun / Feel Of Eternity And Even Fufther</a>
				<div class="b-page__info__wish-list__artist">Pink Floyd</div>
				<div class="b-page__info__wish-list__type">ВИНИЛ, MP3</div>
			</div>
			<a href="javascript:void(0)" class="b-page__info__all" data-oped-wish-list>Весь вишлист (135)</a>

			<div class="b-page__title">Отзывы</div>
			<div class="b-page__info__block">
				<a href="" class="b-page__review__link">Мне понравилось, люди стараются, а еще работает система скидок для...</a>
				<div class="b-page__review__date">Malcovich, 14.07.2013</div>
			</div>
			<div class="b-page__info__block">
				<a href="" class="b-page__review__link">Купил много качественного винила, все в порядке и персонал отвечает на...</a>
				<div class="b-page__review__date">Malcovich, 14.07.2013</div>
			</div>
			<div class="b-page__info__block">
				<a href="" class="b-page__review__link">Хороший продавец, все честно. Рекомендую</a>
				<div class="b-page__review__date">Ilya Goldfarb, 14.07.2013</div>
			</div>
			<a href="javascript:void(0)" class="b-page__info__all" data-oped-review-list>Все отзывы (45)</a>
		</div>
	</div>
	<div class="b-popup__overlay no-generated" data-popup data-wish-list-popup>
		<div class="b-popup">
			<div class="b-popup__header">
				<div class="b-popup__header__avatar">
					<img src="templates/images/_content/avatars/big/1.jpg" />
				</div>
				<div class="b-popup__header__link">
					<a href="javascript:void(0)" class="b-btn is-blue">Перейти к профилю</a>
				</div>
				<div class="b-popup__header__content">
					<div class="b-popup__header__name">Вишлист</div>
					<div class="b-popup__header__user">Steven Baskerville</div>
				</div>
			</div>
			<div class="b-popup__items">
				<a href="item" class="b-popup__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/2.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Bday</div>
						<div class="b-item__text__hint">Beyonce</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="b-popup__overlay no-generated" data-popup data-review-list-popup>
		<div class="b-popup">
			<div class="b-popup__header">
				<div class="b-popup__header__avatar">
					<img src="templates/images/_content/avatars/big/1.jpg" />
				</div>
				<div class="b-popup__header__link">
					<a href="javascript:void(0)" class="b-btn is-blue">Перейти к профилю</a>
				</div>
				<div class="b-popup__header__content">
					<div class="b-popup__header__name">Отзывы <span class="b-popup__header__user">(45)</span></div>
					<div class="b-popup__header__name">Godzilla Vynils</div>
				</div>
			</div>
			<div class="b-popup__list">
				<div class="b-review">
					<div class="b-review__avatar">
						<img src="templates/images/_content/avatars/2.jpg" class="b-review__avatar__img" />
					</div>
					<div class="b-review__content">
						<div class="b-review__header">
							Malcovich, 14.07.2013
						</div>
						<div class="b-review__text">
							Укажите в комментарии к заказу промокод "SSALEE50" и получи скидку 50 процентов на товар!
						</div>
					</div>
				</div>
				<div class="b-review">
					<div class="b-review__avatar">
						<img src="templates/images/_content/avatars/4.jpg" class="b-review__avatar__img" />
					</div>
					<div class="b-review__content">
						<div class="b-review__header">
							Malcovich, 14.07.2013
						</div>
						<div class="b-review__text">
							Только сегодня — невероятная распродажа!
						</div>
					</div>
				</div>
				<div class="b-review">
					<div class="b-review__avatar"></div>
					<div class="b-review__content">
						<div class="b-review__header">
							Malcovich, 14.07.2013
						</div>
						<div class="b-review__text">
							В журнале "Здоровье" за март 1959 года вышел рассказ про 111–летнего сибиряка Исака Макаровича Скопнева
						</div>
					</div>
				</div>
				<div class="b-review">
					<div class="b-review__avatar">
						<img src="templates/images/_content/avatars/5.jpg" class="b-review__avatar__img" />
					</div>
					<div class="b-review__content">
						<div class="b-review__header">
							Malcovich, 14.07.2013
						</div>
						<div class="b-review__text">
							Приглашаем посетить наш онлайн-магазин
							и получить приятные бонусы при первой покупке
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? include('common/social.php');