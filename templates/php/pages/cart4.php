<div class="b-content" data-module-name="Cart">
	<div class="b-page__main-title">Корзина</div>
	<div class="b-page__line">
		<div class="b-page__inner">
			<a href="cart" class="b-page__line__link">Выбор продавца</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<a href="cart2" class="b-page__line__link">Оформление</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<a href="cart3" class="b-page__line__link">Личные данные</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<a href="cart4" class="b-page__line__link">Подтверждение</a>
		</div>
	</div>
	<div class="b-cart-submit">
		<div class="b-page__inner">
			<div class="b-cart-submit__block">
				<div class="b-cart-submit__block__title">Получатель</div>
				<div class="b-cart-submit__block__text">Marry Queen</div>
			</div>
			<div class="b-cart-submit__block">
				<div class="b-cart-submit__block__title">Адрес</div>
				<div class="b-cart-submit__block__text">Temeryazevskaya st, ap 12, Moscow, Russia, 110231</div>
			</div>
			<div class="b-cart-submit__block">
				<div class="b-cart-submit__block__title">Доставка</div>
				<div class="b-cart-submit__block__text">Royal Mail, DHL, Fedex</div>
			</div>
			<div class="b-cart-submit__block">
				<div class="b-cart-submit__block__title">Оплата</div>
				<div class="b-cart-submit__block__text">MasterCard, PayPal</div>
			</div>
		</div>
	</div>
	<div class="b-page__inner">
		<div class="b-big-item-list b-cart-submit-items">
			<div class="b-big-item-list__el" data-item id="1423">
				<div class="b-big-item-list__el__cover">
					<a href="item"><img src="templates/images/_content/items/2.jpg" class="b-big-item-list__el__cover__img" /></a>
				</div>
				<div class="b-big-item-list__el__name-and-ratings">
					<div class="b-big-item-list__el__title"><a href="item" class="b-big-item-list__el__title__link">Tightropewalker + In Like Satin</a></div>
					<div class="b-big-item-list__el__artist">Hamid Baroudi</div>
					<div class="b-big-item-list__el__bottom">
						<a href="seller" class="b-big-item-list__el__user"><i class="icon-user"></i> andrew</a>
					</div>
				</div>
				<div class="b-item-list__el__info">
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">A</span>Save Your Heart</div>
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">L</span>Godzilla Rec</div>
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Y</span>2009</div>
				</div>
				<div class="b-item-list__el__info">
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>M</div>
				</div>
				<div class="b-big-item-list__el__price-and-count">
					<span class="b-big-item-list__el__price">
						89 $
					</span>
					1 шт.
				</div>
			</div>
			<div class="b-big-item-list__el" data-item id="1423">
				<div class="b-big-item-list__el__cover">
					<a href="item"><img src="templates/images/_content/items/3.jpg" class="b-big-item-list__el__cover__img" /></a>
				</div>
				<div class="b-big-item-list__el__name-and-ratings">
					<div class="b-big-item-list__el__title"><a href="item" class="b-big-item-list__el__title__link">Heart Of Sun / Feel Of Eternity And Even Fufther</a></div>
					<div class="b-big-item-list__el__artist">Hamid Baroudi</div>
					<div class="b-big-item-list__el__bottom">
						<a href="seller" class="b-big-item-list__el__user"><i class="icon-user"></i> fred2125</a>
					</div>
				</div>
				<div class="b-item-list__el__info">
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">A</span>Save Your Heart</div>
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">L</span>Godzilla Rec</div>
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Y</span>2009</div>
				</div>
				<div class="b-item-list__el__info">
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
					<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>G</div>
				</div>
				<div class="b-big-item-list__el__price-and-count">
					<span class="b-big-item-list__el__price">
						1 800 $
					</span>
					1 шт.
				</div>
			</div>
		</div>
		<form>
			<div class="b-cart-form__text">
				Проверьте данные. Подтверждая оформление, вы соглашаетесь с <a href="">условиями сервиса</a>.
			</div>
			<div class="b-cart__sum">
				<a href="collection" class="b-cart__sum__link">Продолжить раскопки</a>
				<button type="submit" class="b-btn is-blue is-full-colored">&nbsp; &nbsp; Подтвердить &nbsp; &nbsp;</button>
			</div>
		</form>
	</div>
</div>