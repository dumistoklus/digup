'use strict';

/**
 * @class	app.views.Cart
 * @extends app.views.Default
 */
app.views.Cart = app.views.Default.extend({

	$openedMenu: null,
	isMenuOpen: false,

	events: {
		'click [data-dropdown-option]' 	: '_e_selectOption',
		'click [data-delete-item]'		: '_e_deleteItem',
		'click [data-submit-form]'		: '_e_submitForm',
		'change [data-item-checkbox]'	: '_e_toggleItemStatus'
	},


	initialize: function() {
		var that = this;
		app.dom.$document.on('click.dropdown', function( event ){
			if ( that.isMenuOpen ) {
				that.closeDropDown();
			} else if ( $(event.target).closest('[data-dropdown]').length ) {
				that.openDropDown( $(event.target).closest('[data-dropdown]') );
			}
		});

		this._initCheckboxes();
		this.calculatePrice();
	},


	_initCheckboxes: function() {
		var that = this;
		this.$('[type="checkbox"]').checkbox();
		this.$('[data-item-checkbox]').each(function() {
		   that._e_toggleItemStatus({
			   currentTarget: $(this)
		   });
		});
	},


	openDropDown: function( $menu ) {
		this.isMenuOpen = true;
		this.$openedMenu = $menu;
		$menu.addClass('is-open');
	},


	closeDropDown: function() {
		this.$openedMenu.removeClass('is-open');
		this.$openedMenu = null;
		this.isMenuOpen = false;
	},


	_e_selectOption: function( event ) {
		var $target = $(event.currentTarget);
		var html	= $target.html();
		$target.closest('[data-dropdown]').find('[data-dropdown-value]').html(html);
	},


	_e_deleteItem: function( event ) {
		var that	= this;
		var $target = $(event.currentTarget);
		var $item	= $target.closest('[data-item]');
		var id		= $item.attr('id');

		function removeItem( id ) {
			// TODO: здесь должен быть аякс запрос на удаление из корзины
		}

		removeItem(id);
		$item.slideUp(function(){
			$item.remove();
			that.calculatePrice();
		});
	},


	calculatePrice: function() {
		var price = 0;
		this.$('[data-item]:not(.is-inactive) [data-price]').each(function(){
			price += $(this).data('price');
		});

		this.$('[data-full-price]').text(app.utils.thousandsSeparator(price));
	},


	_e_submitForm: function( event ) {
	    var $target = $(event.currentTarget);
		var $form = $target.closest('form');
		$form.submit();
	},


	_e_toggleItemStatus: function( event ) {
	    var $checkbox = $(event.currentTarget);
		var $item = $checkbox.closest('[data-item]');

		if ( $checkbox.prop('checked') ) {
			$item.removeClass('is-inactive');
		} else {
			$item.addClass('is-inactive');
		}

		this.calculatePrice();
	}
});