'use strict';

/**
 * @class	app.views.Index
 * @extends Backbone.View
 */
app.views.Index = Backbone.View.extend({

	events: {
		'click [data-history-selector]' : '_e_clickHistorySelector'
	},


	initialize: function() {
		this._initBlogParallax();
	},


	/**
	 * Параллакс в блоге
	 * @private
	 */
	_initBlogParallax: function() {
		var $parallax	= this.$('[data-blog-background]');
		var topPos		= 0;
		var height		= 0;

		function resize() {
			topPos = $parallax.offset().top;
			height = $parallax.height();
		}

		function scroll() {
			var top = topPos + height * 1.3 - app.utils.getScrollPosition() - app.Size.height;
			if ( top < 0 ) {
				top = 0;
			}
			$parallax[0].style[Modernizr.transformPrefix] = 'translateY(' + top + 'px)';
		}

		resize();
		scroll();

		app.dom.$window.on('scroll', scroll);
		app.on('resize', resize);
	},


	_e_clickHistorySelector: function( event ) {
		clearTimeout(this.timeout);
		var $target = $(event.target);
		var type = $target.data('history-selector');
		var $newList =  this.$('[data-history="' + type + '"]');
		var $newItems = $newList.find('.b-item');

		// Меняем выделение ссылки на таб
		$(event.target).addClass('is-active').siblings().removeClass('is-active');

		// Переключаем табы
		$newList.removeClass('is-hidden').siblings('[data-history]').addClass('is-hidden');

		// Делаем новые элементы прозрачными
		$newItems.css('opacity', 0);

		// Анимируем появление
		$newItems.each(function() {
			var $element = $(this);
			var timeout = $element.data('timeout');

			clearTimeout(timeout);

			this.offsetHeight; // reflow
			this.style[Modernizr.transitionPrefix + 'Duration'] = '';
			this.offsetHeight; // reflow
			this.style[Modernizr.transitionPrefix + 'Duration'] = '0.4s';
			this.offsetHeight; // reflow

			timeout = setTimeout(function() {
				$element.css('opacity', '');
			}, Math.random() * 400);
			$element.data('timeout', timeout)
		});

		this.timeout = setTimeout(function() {
			$newItems.each(function() {
				this.style[Modernizr.transitionPrefix + 'Duration'] = '';
			});
		}, 1400);
	}
});