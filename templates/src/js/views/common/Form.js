'use strict';

/**
 * @class		app.views.Form
 * @extends 	app.views.Default
 */
app.views.Form = app.views.Default.extend({

	initialize: function() {

		// select init
		this.$('select').ikSelect({
			autoWidth: false,
			ddFullWidth: false,
			onShow: function( inst ) {
				inst.$link.addClass('ik_select_open');
			},
			onHide: function( inst ) {
				inst.$link.removeClass('ik_select_open');
			}
		});

		// checkbox init
		this.$('[type="checkbox"]').checkbox();
	},


	// Свойства нужные для отправки формы
	props: {
		class_for_loader	: '.loader',
		class_for_button	: '.btn',
		class_for_content	: '.form-form',
		class_for_success	: '.form-success',
		class_for_error		: 'has-error'
	},


	_e_submitForm: function(event) {
		var that	= this,
			$form	= $(event.target),
			data,
			url
		;

		if ( that.requiredFieldsNotEmpty( $form ) ) {

			data	= $form.serialize();
			url		= $form.attr('action') || window.location.href;

			$.ajax({
				url			: url,			// override for form's 'action' attribute
				type		:'post',			// 'get' or 'post', override for form's 'method' attribute
				dataType	: 'json',			// 'xml', 'script', or 'json' (expected server response type)
				data		: data,
				complete	: function(){
					that.complete( $form );
				},
				success		: function(data) {
					that.success( $form, data );
				},
				beforeSend	: function () {
					that.beforeSend( $form );
				}
			});
		}

		return false;
	},


	success: function( $form, data ) {

		// Форма заполнена с ошибками
		if ( data.errors ) {
			this.generateErrorMessage( $form, data.errors );
		}
		// Всё окей. Ошибок нет
		else {
			this.successReceived( $form, data );
		}
	},


	beforeSend: function( $form ) {
		$form
			.find('.'+this.props.class_for_error)
			.removeClass(this.props.class_for_error)
			.find('input, textarea')
			.unbind('focus.form')
		;
		$form.find(this.props.class_for_button).hide();
		$form.find(this.props.class_for_loader).show();
	},


	/**
	 * Проверяет заполненность обязательный полей формы
	 * @param $form
	 * @returns {boolean}
	 */
	requiredFieldsNotEmpty: function( $form ) {

		var requiredFieldIsEmpty	= false,
			that					= this
		;

		$form.find('[required="required"]').map(function(){
			if ( $(this).val() === '' ) {
				requiredFieldIsEmpty = true;
				$(this).addClass(that.props.class_for_error).one('focus.required', function(){
					$(this).removeClass(that.props.class_for_error);
				});
			}
		});

		return !requiredFieldIsEmpty;
	},


	/**
	 * Каждому ошибочному полю показывает сообщение об ошибке
	 * @param $form
	 * @param errors
	 */
	generateErrorMessage: function( $form, errors ) {

		var that				= this,
			scrollPositions		= []
		;

		_.each(errors, function(err, name){

			var $item,
				offsetTop
			;

			$item = $form.find('[name="' + name + '"]').closest('.form-group').addClass(that.props.class_for_error);

			if ( $item.length ) {
				offsetTop = $item.offset();
				if ( offsetTop && offsetTop.top ) {
					scrollPositions.push( offsetTop.top );
				}
			}

		});

		if ( scrollPositions.length ) {
			app.utils.scrollTo( _.min(scrollPositions) - 40 );
		}
	},


	/**
	 * Восстанавливает состояние форма и удаляет все сообщения об ошибках
	 */
	complete: function( $form ) {
		$form.find(this.props.class_for_loader).hide();
		$form.find(this.props.class_for_button).show();
	},


	/**
	 * Выполняется, если нет ошибок после отправки формы
	 * @param $form
	 * @param [data]
	 */
	// jshint unused:false
	successReceived: function( $form, data ) {
		var messageType;
		var message;

		if ( window.form_success_messages ) {
			messageType = $form.data('message-type');
			if ( typeof messageType === 'string' && window.form_success_messages[messageType] ) {
				app.utils.showNotification(window.form_success_messages[messageType]);
			}
		}

		$form.find(this.props.class_for_content).slideUp();
		$form.find(this.props.class_for_success).slideDown();
	},


	/**
	 *
	 * @param file			{file}
	 * @param url			{string}
	 * @param [callback]	{function}
	 * @protected
	 */
	_uploadFile: function( file, url, callback ) {
		var that 	= this;
		var xhr		= new XMLHttpRequest();
		var fd		= new FormData();

		function onComplete() {
			that._uploadDone();
			if ( typeof callback === 'function' ) {
				callback();
			}
		}


		that._uploadStart();

		xhr.open('POST', url, true);
		xhr.onreadystatechange = function() {
			if ( xhr.readyState == 4 ) {
				if (xhr.status == 200) {
					that._parseFileUploadResponse(JSON.parse(xhr.responseText));
				}
				onComplete();
			}
		};

		xhr.onerror = onComplete;

		// Устанавливаем заголовки.
		xhr.setRequestHeader('Cache-Control', 'no-cache');
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		fd.append('image', file);
		xhr.send(fd);
	},


	/**
	 * Функция парсит ответ от загрузчика картинок
	 * @param	{Object}	data
	 * @private
	 */
	_parseFileUploadResponse: function( data ) {

	},


	_uploadDone: function() {
		this.$el.removeClass('is-loaded');
	},


	_uploadStart: function() {
		this.$el.addClass('is-loaded');
	}
});