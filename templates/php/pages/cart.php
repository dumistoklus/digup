<div class="b-content" data-module-name="Cart">
	<div class="b-page__main-title">Корзина</div>
	<div class="b-page__line">
		<div class="b-page__inner">
			<a href="cart" class="b-page__line__link">Выбор продавца</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<span class="b-page__line__link">Оформление</span>
			<i class="icon-long-arrow-left"></i>
			<span class="b-page__line__link">Личные данные</span>
			<i class="icon-long-arrow-left"></i>
			<span class="b-page__line__link">Подтверждение</span>
		</div>
	</div>
	<div class="b-cart__item">
		<div class="b-page__inner">
			<div class="b-page__title">
				Товар
			</div>
			<div class="b-page__header__cover">
				<a href="item"><img src="templates/images/_content/items/1.jpg"></a>
			</div>
			<div class="b-page__header__main">
				<div class="b-page__params">
					<div class="b-page__params__dt">Товаров</div>
					<div class="b-page__params__dd">22</div>
					<div class="b-page__params__dt">Сделок</div>
					<div class="b-page__params__dd">28</div>
					<div class="b-page__params__dt">оплата</div>
					<div class="b-page__params__dd">paypal, яндекс.деньги, visa, mastercard</div>
				</div>
				<h1 class="b-page__header__title">
					Godzilla Vinyls
				</h1>
				<div class="b-page__header__subtitle">
					ottawa, canada
				</div>
			</div>
		</div>
	</div>
	<div class="b-page__inner">
		<div class="b-page__title">
			Продавцы
		</div>
		<div class="b-sellers">
			<div class="b-sellers__el">
				<div class="b-sellers__el__avatar">
					<a href="seller"><img src="templates/images/_content/avatars/1.jpg" class="b-sellers__el__avatar__img" /></a>
				</div>
				<div class="b-sellers__el__name-and-rating">
					<div class="b-sellers__el__name"><a href="seller" class="b-sellers__el__name__link">ANDREW</a></div>
					<div class="b-sellers__el__rating"><i class="icon-stat"></i> 9.8</div>
				</div>
				<div class="b-sellers__el__props">
					<div class="b-sellers__el__props__dt">ОПЛАТА</div>
					<div class="b-sellers__el__props__dd">PAYPAL, КАРТА</div>
					<div class="b-sellers__el__props__dt">ДОСТАВКА</div>
					<div class="b-sellers__el__props__dd">ROYAL MAIL, DHL</div>
				</div>
				<div class="b-sellers__el__props2">
					<div class="b-sellers__el__props2__dt">T</div>
					<div class="b-sellers__el__props2__dd">винил</div>
					<div class="b-sellers__el__props2__dt">Q</div>
					<div class="b-sellers__el__props2__dd">G</div>
				</div>
				<div class="b-sellers__el__button">
					<a href="cart2" class="b-btn is-green is-block">В корзину</a>
				</div>
				<div class="b-sellers__el__count-and-price" data-dropdown>
					<div class="b-sellers__el__count-and-price__value" data-dropdown-value><span class="b-sellers__el__price">89 $</span>1шт</div>
					<div class="b-sellers__el__count-and-price__list" data-dropdown-list>
						<div class="b-sellers__el__count-and-price__list__el" data-dropdown-option><span class="b-sellers__el__price">89 $</span>1шт</div>
						<div class="b-sellers__el__count-and-price__list__el" data-dropdown-option><span class="b-sellers__el__price">159 $</span>2шт</div>
					</div>
				</div>
			</div>
			<div class="b-sellers__el">
				<div class="b-sellers__el__avatar">
					<a href="seller"><img src="templates/images/_content/avatars/2.jpg" class="b-sellers__el__avatar__img" /></a>
				</div>
				<div class="b-sellers__el__name-and-rating">
					<div class="b-sellers__el__name"><a href="seller" class="b-sellers__el__name__link">ILYA GOLDFARB</a></div>
					<div class="b-sellers__el__rating"><i class="icon-stat"></i> 4.0</div>
				</div>
				<div class="b-sellers__el__props">
					<div class="b-sellers__el__props__dt">ОПЛАТА</div>
					<div class="b-sellers__el__props__dd">YANDEX Деньги</div>
					<div class="b-sellers__el__props__dt">ДОСТАВКА</div>
					<div class="b-sellers__el__props__dd">Почта России</div>
				</div>
				<div class="b-sellers__el__props2">
					<div class="b-sellers__el__props2__dt">T</div>
					<div class="b-sellers__el__props2__dd">винил</div>
					<div class="b-sellers__el__props2__dt">Q</div>
					<div class="b-sellers__el__props2__dd">P</div>
				</div>
				<div class="b-sellers__el__button">
					<a href="cart2" class="b-btn is-green is-block">В корзину</a>
				</div>
				<div class="b-sellers__el__count-and-price" data-dropdown>
					<div class="b-sellers__el__count-and-price__value" data-dropdown-value><span class="b-sellers__el__price">89 $</span>1шт</div>
					<div class="b-sellers__el__count-and-price__list" data-dropdown-list>
						<div class="b-sellers__el__count-and-price__list__el" data-dropdown-option><span class="b-sellers__el__price">89 $</span>1шт</div>
					</div>
				</div>
			</div>
			<div class="b-sellers__el">
				<div class="b-sellers__el__avatar">
					<a href="seller"><img src="templates/images/_content/avatars/3.jpg" class="b-sellers__el__avatar__img" /></a>
				</div>
				<div class="b-sellers__el__name-and-rating">
					<div class="b-sellers__el__name"><a href="seller" class="b-sellers__el__name__link">ANDREW</a></div>
					<div class="b-sellers__el__rating"><i class="icon-stat"></i> 9.8</div>
				</div>
				<div class="b-sellers__el__props">
					<div class="b-sellers__el__props__dt">ОПЛАТА</div>
					<div class="b-sellers__el__props__dd">PAYPAL, КАРТА</div>
					<div class="b-sellers__el__props__dt">ДОСТАВКА</div>
					<div class="b-sellers__el__props__dd">ROYAL MAIL, DHL</div>
				</div>
				<div class="b-sellers__el__props2">
					<div class="b-sellers__el__props2__dt">T</div>
					<div class="b-sellers__el__props2__dd">винил</div>
					<div class="b-sellers__el__props2__dt">Q</div>
					<div class="b-sellers__el__props2__dd">G</div>
				</div>
				<div class="b-sellers__el__button">
					<a href="cart2" class="b-btn is-green is-block">В корзину</a>
				</div>
				<div class="b-sellers__el__count-and-price" data-dropdown>
					<div class="b-sellers__el__count-and-price__value" data-dropdown-value><span class="b-sellers__el__price">89 $</span>1шт</div>
					<div class="b-sellers__el__count-and-price__list" data-dropdown-list>
						<div class="b-sellers__el__count-and-price__list__el" data-dropdown-option><span class="b-sellers__el__price">89 $</span>1шт</div>
						<div class="b-sellers__el__count-and-price__list__el" data-dropdown-option><span class="b-sellers__el__price">159 $</span>2шт</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>