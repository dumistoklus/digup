<div class="b-content" data-module-name="Deals">
	<div class="b-page__main-title">Сделки</div>
	<div class="b-page__line b-page__line__tabs">
		<a href="javascript:void(0)" class="b-page__line__tab" data-select-tab="0">Покупки</a><!--
		--><a href="javascript:void(0)" class="b-page__line__tab is-selected" data-select-tab="1">Продажи</a>
	</div>
	<div class="b-page__inner">
		<div class="b-deals" data-tab="0">
			<div class="b-deals__row">
				<div class="b-deals__head b-deals__number">Номер</div>
				<div class="b-deals__head b-deals__name">Название товара</div>
				<div class="b-deals__head b-deals__date">Дата заказа</div>
				<div class="b-deals__head b-deals__status">Статус</div>
				<div class="b-deals__head b-deals__send-date">Дата отправки</div>
			</div>

			<div class="b-deals__el is-selected">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#994</div>
					<div class="b-deals__name">Heart Of Sun / Feel Of Eternity And Even Fufther</div>
					<div class="b-deals__date">12.04.2014</div>
					<div class="b-deals__status">В ожидании</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/3.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#847</div>
					<div class="b-deals__name">Tightropewalker + In Like Satin</div>
					<div class="b-deals__date">12.04.2014</div>
					<div class="b-deals__status">В ожидании</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/4.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#377</div>
					<div class="b-deals__name">Inhale Exhale — Movement</div>
					<div class="b-deals__date">21.03.2014</div>
					<div class="b-deals__status">Выставлен счет</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/2.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#377</div>
					<div class="b-deals__name">Inhale Exhale — Movement</div>
					<div class="b-deals__date">21.03.2014</div>
					<div class="b-deals__status">Выставлен счет</div>
					<div class="b-deals__send-date">13.03.2014<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/1.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther And Eternity Of Feel Heart Even
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Написать сообщение</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
					<div class="b-deals__item__block">
					</div>
				</div>
			</div>
		</div>


		<div class="b-deals is-showed" data-tab="1">
			<div class="b-deals__row">
				<div class="b-deals__head b-deals__number">Номер</div>
				<div class="b-deals__head b-deals__name">Название товара</div>
				<div class="b-deals__head b-deals__date">Дата заказа</div>
				<div class="b-deals__head b-deals__status">Статус</div>
				<div class="b-deals__head b-deals__send-date">Дата отправки</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#847</div>
					<div class="b-deals__name">Tightropewalker + In Like Satin</div>
					<div class="b-deals__date">12.04.2014</div>
					<div class="b-deals__status">В ожидании</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/4.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#377</div>
					<div class="b-deals__name">Inhale Exhale — Movement</div>
					<div class="b-deals__date">21.03.2014</div>
					<div class="b-deals__status">Выставлен счет</div>
					<div class="b-deals__send-date">13.03.2014<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/1.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther And Eternity Of Feel Heart Even
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Написать сообщение</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#377</div>
					<div class="b-deals__name">Inhale Exhale — Movement</div>
					<div class="b-deals__date">21.03.2014</div>
					<div class="b-deals__status">Выставлен счет</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/2.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el is-selected">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#994</div>
					<div class="b-deals__name">Heart Of Sun / Feel Of Eternity And Even Fufther</div>
					<div class="b-deals__date">12.04.2014</div>
					<div class="b-deals__status">В ожидании</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/3.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
				</div>
			</div>

			<div class="b-deals__el">
				<a href="javascript:void(0)" class="b-deals__row" data-toggle-deal>
					<div class="b-deals__number">#10 024</div>
					<div class="b-deals__name">Heart Of Sun / Feel Of Eternity And Even Fufther</div>
					<div class="b-deals__date">12.04.2014</div>
					<div class="b-deals__status">В ожидании</div>
					<div class="b-deals__send-date">&nbsp;<i class="icon-angle-down"></i><i class="icon-angle-up"></i></div>
				</a>
				<div class="b-deals__item">
					<div class="b-deals__number b-deals__item__block">
						<img src="templates/images/_content/items/3.jpg" class="b-deals__item__cover" />
					</div>
					<div class="b-deals__name b-deals__item__block b-deals__item__info">
						Heart Of Sun / Feel Of Eternity And Even Fufther
						<div class="b-deals__item__info__artist">Hamid Baroudi</div>
						<a href="user" class="b-deals__item__info__user"><i class="icon-user"></i> andrew</a>
					</div>
					<div class="b-deals__date b-deals__item__block b-deals__item__spec">
						<div class="b-deals__item__spec__dt">T</div>
						<div class="b-deals__item__spec__dd">Винил</div>
						<div class="b-deals__item__spec__dt">Q</div>
						<div class="b-deals__item__spec__dd">M</div>
						<a href="messages" class="b-deals__item__spec__message b-btn is-blue">Сообщения (4)</a>
					</div>
					<div class="b-deals__status b-deals__item__block">
						<span class="b-deals__item__price">89 $</span>
						<span class="b-deals__item__count">1 шт</span>
					</div>
					<div class="b-deals__el__actions">
						<a href="javascript:void(0)" class="b-deals__el__actions__current" data-open-actions-list>В ожидании <i class="icon-angle-down"></i><i class="icon-angle-up"></i></a>
						<div class="b-deals__el__actions__options">
							<a href="javascript:void(0)" class="b-deals__el__actions__options__el">В обработке</a>
							<a href="javascript:void(0)" class="b-deals__el__actions__options__el">Выставить счет <i class="icon-arrow-right"></i></a>
							<a href="javascript:void(0)" class="b-deals__el__actions__options__el">Отправлено</a>
							<a href="javascript:void(0)" class="b-deals__el__actions__options__el">Отклонено</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>