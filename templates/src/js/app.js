'use strict';

var app = {

	root		: $('base').attr('href'),

	// Содержит view для шапки сайта
	HeaderView	: null,

	// Содержит view текущего состояния
	CurrentView	: null,

	// Коллекция утилит
	utils		: {},

	dom			: {
		$window 	: $(window),
		$document	: $(document),
		$body		: $('body'),
		$main		: $('.b-main-container')
	},

	/**
	 * @namespace
	 */
	views		: {},

	/**
	 * @namespace
	 */
	modules		: {}
};

_.extend(app, Backbone.Events);
