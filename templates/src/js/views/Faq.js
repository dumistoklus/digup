'use strict';

/**
 * @class	app.views.Faq
 * @extends app.views.Form
 */
app.views.Faq = app.views.Form.extend({

	_timeout: 0,

	events: {
		'click [data-question]'	: '_e_openAnswer',
		'submit form'			: '_e_submitForm'
	},


	_e_openAnswer: function( event ) {
		clearTimeout(this._timeout);
		var $target = $(event.currentTarget);
		var index = $target.index();
		var $container = this.$('[data-answers]');
		var $answers = $container.children();
		var $answer = $answers.eq(index);
		var height;
		var containerHeight;
		var maxHeight;

		$answer.css({
			display: 'block'
		});
		height = $answer.height();
		containerHeight = $container.height();
		maxHeight = Math.max(containerHeight, height);

		$container.css({
			height: maxHeight
		});
		$answers.removeClass('is-current');
		$answer.addClass('is-current');

		this._timeout = setTimeout(function() {
			$container.css({
				height: ''
			});
		}, 400);

		// Пункты меню
		$target.addClass('is-current').siblings().removeClass('is-current');
	}
});