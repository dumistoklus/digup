<div class="b-market__menu__form__layer">
	<div class="b-form__title">
		Сортировать
	</div>
	<div class="b-form-field">
		<select name="order">
			<option value="date">По дате добавления</option>
			<option value="price">По цене</option>
		</select>
	</div>
	<div class="b-form-field">
		<label><input type="checkbox" name="with_audition">С прослушиванием</label>
	</div>
</div>
<div class="b-market__menu__form__layer is-contains-full-checkboxes">
	<div class="b-form__title">
		<a href="javascript:void(0)" class="b-market__menu__form__button" data-show-full><i class="icon-angle-right"></i><i class="icon-angle-left"></i></a>
		По жанру
	</div>
	<div class="b-form__checkboxes">
		<label><input type="checkbox" name="type[3]">Electronic</label>
		<label><input type="checkbox" name="type[2]">Rock</label>
		<label><input type="checkbox" name="type[1]">Funk / Soul</label>
		<label><input type="checkbox" name="type[0]">Classical</label>
	</div>
	<div class="b-form__checkboxes b-market__menu__form__full-checkboxes" data-full-checkboxes>
		<label><input type="checkbox" name="type[3]">Electronic</label>
		<label><input type="checkbox" name="type[2]">Rock</label>
		<label><input type="checkbox" name="type[1]">Funk / Soul</label>
		<label><input type="checkbox" name="type[0]">Classical</label>
		<label><input type="checkbox" name="type[4]">Light</label>
		<label><input type="checkbox" name="type[5]">Buss</label>
		<label><input type="checkbox" name="type[6]">Chill</label>
		<label><input type="checkbox" name="type[7]">Pop</label>
	</div>
</div>
<div class="b-market__menu__form__layer is-contains-full-checkboxes">
	<div class="b-form__title">
		<a href="javascript:void(0)" class="b-market__menu__form__button" data-show-full><i class="icon-angle-right"></i><i class="icon-angle-left"></i></a>
		По стилю
	</div>
	<div class="b-form__checkboxes">
		<label><input type="checkbox" name="style[3]">House</label>
		<label><input type="checkbox" name="style[2]">Techno</label>
		<label><input type="checkbox" name="style[1]">Disco</label>
		<label><input type="checkbox" name="style[0]">Tech</label>
	</div>
	<div class="b-form__checkboxes b-market__menu__form__full-checkboxes" data-full-checkboxes>
		<div class="b-market__menu__form__full-checkboxes__half">
			<label><input type="checkbox" name="style[3]">House</label>
			<label><input type="checkbox" name="style[2]">Techno</label>
			<label><input type="checkbox" name="style[1]">Disco</label>
			<label><input type="checkbox" name="style[0]">Tech</label>
			<label><input type="checkbox" name="style[3]">House</label>
			<label><input type="checkbox" name="style[2]">Techno</label>
			<label><input type="checkbox" name="style[1]">Disco</label>
			<label><input type="checkbox" name="style[0]">Tech</label>
		</div>
		<div class="b-market__menu__form__full-checkboxes__half">
			<label><input type="checkbox" name="style[3]">House</label>
			<label><input type="checkbox" name="style[2]">Techno</label>
			<label><input type="checkbox" name="style[1]">Disco</label>
			<label><input type="checkbox" name="style[0]">Tech</label>
			<label><input type="checkbox" name="style[3]">House</label>
			<label><input type="checkbox" name="style[2]">Techno</label>
			<label><input type="checkbox" name="style[1]">Disco</label>
			<label><input type="checkbox" name="style[0]">Tech</label>
		</div>
	</div>
</div>
<div class="b-market__menu__form__layer is-contains-full-checkboxes">
	<div class="b-form__title">
		<a href="javascript:void(0)" class="b-market__menu__form__button" data-show-full><i class="icon-angle-right"></i><i class="icon-angle-left"></i></a>
		По десятилетию
	</div>
	<div class="b-form__checkboxes">
		<label><input type="checkbox" name="age[3]">00-13</label>
		<label><input type="checkbox" name="age[2]">80-90</label>
		<label><input type="checkbox" name="age[1]">60-70</label>
		<label><input type="checkbox" name="age[0]">40-50</label>
	</div>
	<div class="b-form__checkboxes b-market__menu__form__full-checkboxes" data-full-checkboxes>
		<div class="b-market__menu__form__full-checkboxes__half">
			<label><input type="checkbox" name="age[3]">до 1910</label>
			<label><input type="checkbox" name="age[2]">10-20</label>
			<label><input type="checkbox" name="age[1]">20-30</label>
			<label><input type="checkbox" name="age[0]">30-40</label>
			<label><input type="checkbox" name="age[3]">40-50</label>
		</div>
		<div class="b-market__menu__form__full-checkboxes__half">
			<label><input type="checkbox" name="age[2]">50-60</label>
			<label><input type="checkbox" name="age[1]">60-70</label>
			<label><input type="checkbox" name="age[0]">70-80</label>
			<label><input type="checkbox" name="age[0]">90-00</label>
			<label><input type="checkbox" name="age[0]">00-14</label>
		</div>
	</div>
</div>
<div class="b-market__menu__form__layer is-contains-full-checkboxes">
	<div class="b-form__title">
		<a href="javascript:void(0)" class="b-market__menu__form__button" data-show-full><i class="icon-angle-right"></i><i class="icon-angle-left"></i></a>
		По стране
	</div>
	<div class="b-form__checkboxes">
		<label><input type="checkbox" name="country[3]">США</label>
		<label><input type="checkbox" name="country[2]">Мексика</label>
		<label><input type="checkbox" name="country[1]">Италия</label>
		<label><input type="checkbox" name="country[0]">Канада</label>
	</div>
	<div class="b-form__checkboxes b-market__menu__form__full-checkboxes" data-full-checkboxes>
		<div class="b-market__menu__form__full-checkboxes__half">
			<label><input type="checkbox" name="country[3]">США</label>
			<label><input type="checkbox" name="country[2]">Мексика</label>
			<label><input type="checkbox" name="country[1]">Италия</label>
			<label><input type="checkbox" name="country[0]">Канада</label>
			<label><input type="checkbox" name="country[0]">Россия</label>
			<label><input type="checkbox" name="country[0]">Германия</label>
			<label><input type="checkbox" name="country[0]">Австрия</label>
			<label><input type="checkbox" name="country[0]">Польша</label>
			<label><input type="checkbox" name="country[0]">Кения</label>
			<label><input type="checkbox" name="country[0]">Уганда</label>
		</div>
		<div class="b-market__menu__form__full-checkboxes__half">
			<label><input type="checkbox" name="country[0]">Зимбабве</label>
			<label><input type="checkbox" name="country[0]">Испания</label>
			<label><input type="checkbox" name="country[0]">Япония</label>
			<label><input type="checkbox" name="country[0]">Новая Зеландия</label>
			<label><input type="checkbox" name="country[0]">Австралия</label>
			<label><input type="checkbox" name="country[0]">Сингапур</label>
			<label><input type="checkbox" name="country[0]">Китай</label>
			<label><input type="checkbox" name="country[0]">Бельгия</label>
			<label><input type="checkbox" name="country[0]">Чехия</label>
			<label><input type="checkbox" name="country[0]">Чили</label>
		</div>
	</div>
</div>