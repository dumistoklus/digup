<div class="b-content" data-module-name="Cart">
	<div class="b-page__main-title">Корзина</div>
	<div class="b-page__line">
		<div class="b-page__inner">
			<a href="cart" class="b-page__line__link">Выбор продавца</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<a href="cart2" class="b-page__line__link">Оформление</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<a href="cart3" class="b-page__line__link">Личные данные</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<span class="b-page__line__link">Подтверждение</span>
		</div>
	</div>
	<form action="cart4.php">
		<div class="b-page__inner">
			<div class="b-cart-form">
				<div class="b-cart-form__half">
					<div class="b-page__title">
						Личные данные
					</div>
					<label class="b-form-field">
						<input type="text" name="first_name" placeholder="Имя" required>
					</label>
					<label class="b-form-field">
						<input type="text" name="last_name" placeholder="Фамилия" required>
					</label>
				</div>
				<div class="b-cart-form__half">
					<div class="b-page__title">
						Адрес доставки
					</div>
					<label class="b-form-field">
						<input type="text" name="country" placeholder="Страна" required>
					</label>
					<label class="b-form-field">
						<input type="text" name="city" placeholder="Город" required>
					</label>
					<label class="b-form-field">
						<input type="text" name="zipcode" placeholder="Индекс">
					</label>
					<label class="b-form-field">
						<input type="text" name="address" placeholder="Улица, дом, квартира / офис" required>
					</label>
				</div>
			</div>
			<div class="b-cart-form__text">
				Заполните все данные, необходимые для доставки товара. Подтверждая оформление, вы соглашаетесь с <a href="">условиями сервиса</a>.
			</div>
			<div class="b-cart__sum">
				<a href="collection" class="b-cart__sum__link">Продолжить раскопки</a>
				<button type="submit" class="b-btn is-blue is-full-colored">&nbsp; &nbsp; Все верно &nbsp; &nbsp;</button>
			</div>
		</div>
	</form>
</div>