'use strict';

/**
 * @class	app.views.Item
 * @extends app.views.Default
 */
app.views.Item = app.views.Default.extend({

	_lastTrackId			: 0,
	_currentPlayedTrackId	: null,
	events					: {
		'click [data-player]'			: '_e_toggleTrack',
		'click [data-show-full-image]'	: '_e_showCover',
		'click [data-add-in-watch-list]': '_e_addInWatchList',
		'click [data-add-in-owner-list]': '_e_addInOwnerList',
		'mousemove [data-item-header]'	: '_e_headerMouseMove'
	},


	initialize: function() {
		this._initMouseMove();
	},


	showCover: function( html ) {
		var $overlay = $('<div class="b-overlay" />');
		var $popup = $(html);

		// Добавдяем попап в DOM
		app.dom.$body.append($overlay);
		app.dom.$body.append($popup);

		// Показываем сначала оверлей, затем содержимое
		$overlay.fadeIn(function(){
			$popup.animate({
				opacity: 1
			});
		});

		function closePopup() {
			$overlay.add($popup).fadeOut(function(){
				$(this).remove();
			});
		}

		// События для оверлея
		$overlay.click(closePopup);
	},


	_initMouseMove: function() {
		this._$header = this.$('[data-item-header-cover]');
		this._$blueOnCover = this.$('[data-item-header-cover-inner]');
		this._mouseMoveResize();
		this.listenTo(app, 'resize', this._mouseMoveResize);
	},


	_mouseMoveResize: function() {
		this._headerWidth	= this._$header.width();
		this._coverLeft		= this._$header.offset().left;
	},


	_e_toggleTrack: function( event ) {
		var $target	= $(event.currentTarget);
		var trackId	= $target.data('track-id');

		// Отмечаем трек
		if ( !trackId ) {
			$target.data('track-id', ++this._lastTrackId );
			trackId = this._lastTrackId;
		}

		// маеняет вижуал любых других треков на дефолный
		this.$('[data-player]').removeClass('is-played');

		// Закпскаем или останавливаем трек
		if ( trackId === this._currentPlayedTrackId ) {
			this.pause();
			this._currentPlayedTrackId = null;
		} else {
			this.play($target.data('url'));
			this._currentPlayedTrackId = trackId;
			$target.addClass('is-played');
		}
	},


	_e_headerMouseMove: function( event ) {
		var diff = this._coverLeft > event.pageX ? this._coverLeft - event.pageX : event.pageX - this._coverLeft - this._headerWidth;
		var opacity = 1 - Math.max(0, Math.min( diff / (1000 / 2), 1));
		this._$blueOnCover.css('background', 'rgba(16, 56, 186, ' + opacity * 0.9 + ')');
	},


	_e_showCover: function( event ) {
		var $target = $(event.target);
		if ( $target.filter('[data-show-full-image]').length || $target.hasClass('icon-magnifier') ) {
			$target		= $(event.currentTarget);
			var address = $target.attr('href');
			var html	= '<div class="b-popup-cover"><img src="' + address + '" /></div>';

			this.showCover(html);
		}
		return false;
	},


	_e_addInWatchList: function( event ) {
		// TODO: Добавить запрос
		var $target = $(event.currentTarget);
		var $counter = this.$('[data-wish-list-count]');
		var count = parseInt($counter.text().replace(' ', ''));
		var isSelected = $target.hasClass('is-selected');
		$target.toggleClass('is-selected');
		count = isSelected ? count - 1 : count + 1;
		$counter.text(app.utils.thousandsSeparator(count));
		app.utils.showNotification(isSelected ? 'Товар убран из отслеживаемых' : 'Товар добавлен в отслеживаемые');
	},


	_e_addInOwnerList: function( event ) {
		// TODO: Добавить запрос
		var $target = $(event.currentTarget);
		var isSelected = $target.hasClass('is-selected');
		var $counter = this.$('[data-owner-list-count]');
		var count = parseInt($counter.text().replace(' ', ''));
		$target.toggleClass('is-selected');
		count = isSelected ? count - 1 : count + 1;
		$counter.text(app.utils.thousandsSeparator(count));
		app.utils.showNotification(isSelected ? 'Вы вышли из списока владельцев' : 'Вы добавлены в список владельцев');
	}
});