'use strict';

app.utils = {
	getHeightContainer: function($targetBox) {
		var $contentHeight;

		$targetBox.css('height', 'auto'); 				                	// Для анимирования высоты контейнера со значение 'auto' сначала быстро проверим его высоту
		$contentHeight 	= $targetBox.outerHeight(); 		            	// высота целевого блока

		$targetBox.css('height', '0');					                	// Вернём обратно значение в '0'

		return $contentHeight;
	},


	/**
	 * Парсит GET. Возвращает объект в виде ключ -> значение
	 * @returns {Object}
	 */
	parseUrlQuery: function( link ) {
		var qu		= link && link.indexOf('?'),
			data	= {},
			i,
			param,
			pair
			;

		if ( link ) {
			if ( qu !== -1 ) {
				link = link.substr(qu + 1);
			}
		} else {
			link = location.search && location.search.substr(1);
		}

		if ( link ) {
			pair = ( link ).split('&');
			for ( i = 0; i < pair.length; i ++ ) {
				param = pair[i].split('=');
				data[param[0]] = param[1];
			}
		}
		return data;
	},

	/**
	 * Возвращает текущую позицию скрола
	 * @returns {Number}
	 */
	getScrollPosition: function() {
		if ( window.scrollY !== undefined ) {
			return window.scrollY;
		} else if ( document.documentElement && document.documentElement.scrollTop ) { // for ie
			return document.documentElement.scrollTop;
		}
		return 0;
	},


	thousandsSeparator: function( num ) {
		num = num.toString();
		var km;
		var j;
		if( (j = num.length) > 3 ){
			 j = j % 3;
		} else{
			j = 0;
		}
		km = (j ? num.substr(0, j) + ' ' : "");
		return km + num.substr(j).replace(/(\d{3})(?=\d)/g, "$1 ");
	}
};