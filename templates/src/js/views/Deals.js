'use strict';

/**
 * @class	app.views.Deals
 * @extends app.views.Default
 */
app.views.Deals = app.views.Default.extend({

	events: {
		'click [data-toggle-deal]'		: '_e_toggleDeal',
		'click [data-select-tab]'		: '_e_selectTab',
		'click [data-open-actions-list]': '_e_openActionsList'
	},


	_e_toggleDeal: function( event ) {
		var $target = $(event.currentTarget);
		var $parent = $target.parent();
		if ( !$parent.hasClass('is-selected') ) {
			setTimeout(function(){
				$parent.addClass('no-overflow');
			}, 400);
		} else {
			$parent.removeClass('no-overflow');
		}
		$parent.toggleClass('is-selected').siblings().removeClass('is-selected no-overflow');
	},


	_e_openActionsList: function( event ) {
		var $target = $(event.currentTarget);

		if ( !$target.closest('.is-open-list').length ) {

			$target.parent().addClass('is-open-list');
			app.dom.$document.on('click.list', function () {
				app.dom.$document.off('click.list');
				$target.parent().removeClass('is-open-list');
			});

			return false;
		}
	}
});