<?
$items = array(
	'avatar'	=> 'templates/images/_content/avatars/big/1.jpg',
	'time'		=> 'сегодня в 12:32',
	'hint'		=> 'Добавлено товаров: 8',
	'name'		=> 'Godzilla Vinyls',
	'items'		=> array(
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/1.jpg',
			'name'		=> 'Off The Deep End',
			'artist'	=> 'Weird All Yankivic',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '35'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/2.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/3.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/3.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/3.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/3.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/3.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		),
		array(
			'url'		=> 'item',
			'image'		=> 'templates/images/_content/items/3.jpg',
			'name'		=> 'Bday',
			'artist'	=> 'Beyonce',
			'label'		=> 'My Super Label',
			'year'		=> '2009',
			'type'		=> 'винил',
			'quality'	=> 'p',
			'price'		=> '45'
		)
	)
);
?>
<div class="b-content" data-module-name="Stream" data-url="stream">
	<div class="b-page__inner b-stream">

		<? // По данному комментарию отрезается контент. Не удалять, если нужен ajax ?>
		<!-- ITEMS -->
		<div class="b-stream__half" data-half>
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<span class="b-stream__block__header__link">все 10 <i class="icon-arrow-right"></i></span>
					<div class="b-stream__block__header__sign">
						<i class="icon-update"></i>
					</div>
					<div class="b-stream__block__header__title">
						Bercey charles:
						<div class="b-stream__block__header__sub-title">
							Добавлено товаров: 10, сегодня в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__items has-three">
					<div class="b-stream__items__inner"><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/1.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Funky Town</div>
							<div class="b-stream__items__el__artist b-stream__artist">Prince</div>
						</div><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/2.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Dark Side Of The Moon</div>
							<div class="b-stream__items__el__artist b-stream__artist">Pink Floyd</div>
						</div><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/3.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Park Avenue</div>
							<div class="b-stream__items__el__artist b-stream__artist">Pink Floyd</div>
						</div><!--
						--><div class="b-stream__items__el__overlay b-stream__overlay">
							<div class="b-stream__overlay__inner">
								посмотреть <br />
								все товары <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
							</div>
						</div><!--
					--></div>
				</div>
			</a>
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<div class="b-stream__block__header__sign">
						<i class="icon-quote"></i>
					</div>
					<div class="b-stream__block__header__title">
						Godzilla Vinyls:
						<div class="b-stream__block__header__sub-title">
							21.04 в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__text">
					Сегодня у нас распродажа — все по 300 руб! Много качественного товара, хорошие цены и продавцы
				</div>
			</a>
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<span class="b-stream__block__header__link">все 10 <i class="icon-arrow-right"></i></span>
					<div class="b-stream__block__header__sign">
						<i class="icon-update"></i>
					</div>
					<div class="b-stream__block__header__title">
						Bercey charles:
						<div class="b-stream__block__header__sub-title">
							Добавлено товаров: 10, сегодня в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__items">
					<div class="b-stream__items__inner"><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/1.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Funky Town</div>
							<div class="b-stream__items__el__artist b-stream__artist">Prince</div>
						</div><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/2.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Dark Side Of The Moon</div>
							<div class="b-stream__items__el__artist b-stream__artist">Pink Floyd</div>
						</div><!--
						--><div class="b-stream__items__el__overlay b-stream__overlay">
							<div class="b-stream__overlay__inner">
								посмотреть <br />
								все товары <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
							</div><!--
						--></div>
					</div>
				</div>
			</a>
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<div class="b-stream__block__header__sign">
						<i class="icon-quote"></i>
					</div>
					<div class="b-stream__block__header__title">
						Godzilla Vinyls:
						<div class="b-stream__block__header__sub-title">
							21.04 в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__text">
					При покупке онлайн более 5 товаров — шестой винил в подарок! Празднуем день открытия нашего магазина Godzilla
				</div>
				<div class="b-stream__one__image-container">
					<img src="templates/images/_content/stream/1.jpg" class="b-stream__one__image-container__image">
					<div class="b-stream__overlay">
						<div class="b-stream__overlay__inner">
							перейти к<br />
							superockets shop <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="b-stream__half" data-half>
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<span class="b-stream__block__header__link">все 10 <i class="icon-arrow-right"></i></span>
					<div class="b-stream__block__header__sign">
						<i class="icon-update"></i>
					</div>
					<div class="b-stream__block__header__title">
						Bercey charles:
						<div class="b-stream__block__header__sub-title">
							Добавлено один товар, сегодня в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__items has-three">
					<div class="b-stream__one__img-container">
						<img src="templates/images/_content/items/1.jpg">
						<div class="b-stream__overlay">
							<div class="b-stream__overlay__inner">
								перейти к<br />
								Bercey charles <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
							</div>
						</div>
					</div>
					<div class="b-stream__one__data">
						<div class="b-stream__one__data__name b-stream__name">Park Avenue</div>
						<div class="b-stream__one__data__artist b-stream__artist">Pink Floyd</div>
					</div>
				</div>
			</a>
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<div class="b-stream__block__header__sign">
						<i class="icon-quote"></i>
					</div>
					<div class="b-stream__block__header__title">
						Godzilla Vinyls:
						<div class="b-stream__block__header__sub-title">
							21.04 в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__text">
					При покупке онлайн более 5 товаров — шестой винил в подарок! Празднуем день открытия нашего магазина Godzilla
				</div>
				<div class="b-stream__one__image-container">
					<img src="templates/images/_content/stream/1.jpg" class="b-stream__one__image-container__image">
					<div class="b-stream__overlay">
						<div class="b-stream__overlay__inner">
							перейти к<br />
							superockets shop <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
						</div>
					</div>
				</div>
			</a>
			<!-- этот тип уже был -->
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<span class="b-stream__block__header__link">все <i class="icon-arrow-right"></i></span>
					<div class="b-stream__block__header__sign">
						<i class="icon-eye"></i>
					</div>
					<div class="b-stream__block__header__title">
						поступления в жанре: <span class="b-stream__block__header__title__style">Rock</span>
						<div class="b-stream__block__header__sub-title">
							19.04 в 12:00
						</div>
					</div>
				</div>
				<div class="b-stream__mini-items">
					<img src="templates/images/_content/items/small/1.jpg" class="b-stream__mini-items__img">
					<img src="templates/images/_content/items/small/2.jpg" class="b-stream__mini-items__img">
					<img src="templates/images/_content/items/small/3.jpg" class="b-stream__mini-items__img">
					<img src="templates/images/_content/items/small/4.jpg" class="b-stream__mini-items__img">
					<div class="b-stream__overlay">
						<div class="b-stream__overlay__inner">
							Посмотреть все <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
						</div>
					</div>
				</div>
			</a>
			<!-- этот тип уже был -->
			<a href="javascript:void(0)" class="b-stream__block" data-stream-block data-items='<?= json_encode($items) ?>'>
				<div class="b-stream__block__header">
					<span class="b-stream__block__header__link">все 10 <i class="icon-arrow-right"></i></span>
					<div class="b-stream__block__header__sign">
						<i class="icon-update"></i>
					</div>
					<div class="b-stream__block__header__title">
						Bercey charles:
						<div class="b-stream__block__header__sub-title">
							Добавлено товаров: 10, сегодня в 12:32
							Добавлено товаров: 10, сегодня в 12:32
						</div>
					</div>
				</div>
				<div class="b-stream__items has-three">
					<div class="b-stream__items__inner"><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/1.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Funky Town</div>
							<div class="b-stream__items__el__artist b-stream__artist">Prince</div>
						</div><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/2.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Dark Side Of The Moon</div>
							<div class="b-stream__items__el__artist b-stream__artist">Pink Floyd</div>
						</div><!--
						--><div class="b-stream__items__el">
							<img src="templates/images/_content/items/3.jpg" class="b-stream__items__el__img">
							<div class="b-stream__items__el__name b-stream__name">Park Avenue</div>
							<div class="b-stream__items__el__artist b-stream__artist">Pink Floyd</div>
						</div><!--
						--><div class="b-stream__items__el__overlay b-stream__overlay">
							<div class="b-stream__overlay__inner">
								посмотреть <br />
								все товары <div class="b-stream__items__el__next-container">&nbsp;<div class="b-stream__items__el__next-inner"><i class="icon-arrow-right"></i></div></div>
							</div>
						</div><!--
					--></div>
				</div>
			</a>
		</div>

		<? // По данному комментарию отрезается контент. Не удалять, если нужен ajax ?>
		<!-- / ITEMS -->


		<a href="javascript:void(0)" class="b-more-link" data-more>Показать больше <i class="icon-arrow-down"></i></a>
		<div class="b-loader"></div>
	</div>
</div>