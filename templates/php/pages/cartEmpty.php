<div class="b-content" data-module-name="Cart">
	<div class="b-page__main-title">Корзина</div>
	<div class="b-page__line">
		<div class="b-page__inner is-center-text">
			<a href="cart" class="b-page__line__link">Оформление</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<span class="b-page__line__link">Личные данные</span>
			<i class="icon-long-arrow-left"></i>
			<span class="b-page__line__link">Подтверждение</span>
		</div>
	</div>
	<div class="b-page__inner">
		<div class="b-cart__empty">
			<div class="b-cart__empty__text">
				Корзина пуста. Чтобы найти товар, <br />
				перейдите к раскопкам или воспользуйтесь поиском
			</div>
			<a href="collection" class="b-btn is-blue is-full-colored">Перейти к раскопкам</a>
		</div>
	</div>
</div>