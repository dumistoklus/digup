'use strict';

/**
 * @class	app.views.AddRelease
 * @extends app.views.Default
 */
app.views.AddRelease = app.views.Form.extend({

	events: {
		'click .b-release'			: '_e_selectRelease',
		'change input[type=file]'	: '_e_addedFileInInput',
		'click [data-add-track]'	: '_addTrack'
	},


	initialize: function() {
		app.dom.$body.addClass('has-big-input');
		app.views.AddRelease.__super__.initialize.apply(this, arguments);
		this._initAutoCompletes();
		this._$cover = this.$('[data-cover-image]');
		this._$traks = this.$('[data-tracks-list]');
		this._trackTemplate = this.$('[data-track-template]').text();
		this._lastTrackNum = this._$traks.children().length;
		this._addTrack();
	},


	_initAutoCompletes: function() {
		this._initAutoComplete( this.$('[name="artist"][type="text"]'), 'searchArtistJson.php' );
		this._initAutoComplete( this.$('[name="album"][type="text"]'), 'searchArtistJson.php' );
		this._initAutoComplete( this.$('[name="label"][type="text"]'), 'searchArtistJson.php' );
		this._initAutoComplete( this.$('[name="country"][type="text"]'), 'searchArtistJson.php' );
		this._initAutoComplete( this.$('[name="genre"][type="text"]'), 'searchArtistJson.php' );
	},


	_initAutoComplete: function( $input, url ) {
		var $hiddenInput	= $input.prev();
		var lastValue		= $input.val();

		$input.autocomplete({
			appendTo: $input.parent(),
			source	: url,
			select	: function( event, ui ) {
				$input.val(ui.item.label);
				$hiddenInput.val(ui.item.value);
				lastValue = ui.item.label;
				return false;
			},
			focus	: function( event, ui ) {
				$input.val(ui.item.label);
				$hiddenInput.val(ui.item.value);
				lastValue = ui.item.label;
				return false;
			}
		});

		$input.on('change', function() {
			if ( lastValue !== $input.val() ) {
				$hiddenInput.val('');
			}
		});
	},


	_e_selectRelease: function( event ) {
		var $target = $(event.currentTarget);
		$target.addClass('is-active').siblings().removeClass('is-active');
		$target.find('[type="text"]').focus();
	},


	_e_addedFileInInput: function( event ) {
		var that = this;
		var address = $(event.currentTarget).data('upload-file-url');

		$.each(event.target.files, function(i, v) {
			var fr	= new FileReader();
			fr.file	= v;
			fr.readAsDataURL(v);

			// Загружаем файл
			that._uploadFile(fr.file, address);
		});
	},


	/**
	 * Парсинг ответа успешной или не успешной загрузки аватарки
	 * @param	{Object}	response
	 * @protected
	 */
	_parseFileUploadResponse: function( response ) {

		// Все нормально загрузилось
		if ( response.item && response.item['^cover'] ) {
			this._$cover.attr('src', response.item['^cover'] );
			this.$el.addClass('is-avatar-loaded');
		}

		// Обработчик ошибок
		else if ( response.errors && response.errors.length > 0 ) {
			_.each(response.errors, function(error){
				app.utils.showError(error);
			});
		}
	},


	_addTrack: function() {
		this._lastTrackNum++;
		var that = this;
		var template = this._trackTemplate.replace(/%num%/g, this._lastTrackNum);
		var $selector;

		// Вставляем новую строку с треком
		this._$traks.append(template);

		// Создаем автокомплит на выборе артиста
		$selector = this.$('[data-artist-selector]');
		$selector.each(function(){
			var $this = $(this);
			that._initAutoComplete( $this, 'searchArtistJson.php' );
			$this.removeAttr('data-artist-selector');
		});

	}
});