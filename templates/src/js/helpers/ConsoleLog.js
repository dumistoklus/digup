'use strict';

/**
 * Быстроконосоль
 * @version 1.0.1
 */
window.ConsoleLog = {

	_console	: null,
	_count		: 0,
	_maxCount	: 18,

	log: function (string) {
		if (!this._console) {
			this._createConsole();
		}
		this._appendString(String(string));
	},

	_appendString: function (string) {
		$(this._console).prepend('<div style="margin-bottom: 5px">' + string + '</div>');
		this._count++;
		if (this._count > this._maxCount) {
			$(this._console).children(':last').remove();
		}
	},

	_createConsole: function () {
		this._console = $('body').append('<div style="padding: 5px; color: #000; font-size: 11px; background: #fff; position: fixed; bottom: 5px; z-index: 10000; left: 5px; max-width: 200px; line-height: 1.1;" />').children(':last')[0];
	}
};