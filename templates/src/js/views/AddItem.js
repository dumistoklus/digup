'use strict';

/**
 * @class	app.views.AddItem
 * @extends app.views.Default
 */
app.views.AddItem = app.views.Form.extend({

	currentArtistId	: null,
	currentAlbumId	: null,
	_lastArtistQuery: '',


	events: {
		'submit [data-artists-form]': '_e_findArtists',
		'submit [data-albums-form]'	: '_e_findAlbums',
		'click [data-artist]'		: '_e_selectArtist',
		'click [data-album]'		: '_e_selectAlbum',
		'click [data-form-submit]'	: '_e_submitForm',
		'click [data-close-album]'	: '_closeAlbum',
		'click [data-close-artist]'	: '_closeArtist'
	},


	initialize: function() {
		this._$artists = this.$('[data-artists]');
		app.views.AddItem.__super__.initialize.apply(this, arguments);
	},


	_e_submitForm: function( event ) {
	    $(event.currentTarget).closest('form').submit();
	},


	_e_findArtists: function( event ) {
		var that 	= this;
		var $form	= $(event.currentTarget);
		var data 	= {
			query		: $(event.currentTarget).find('[name="name"]').val()
		};

		this._$artists.addClass('has-border');

		// Если аналогичного запроса еще не было, отправляем этот
		if ( data.query !== this._lastArtistQuery  ) {
			// Добавляем класс "has-border", для того чтобы появился бордер
			this._$artists.addClass('is-search has-border').html('');
			this.currentAlbumId		= null;
			this._lastArtistQuery	= data.query;

			$.ajax({
				url			: $form.attr('action'),
				type		: 'post',
				dataType	: 'html',
				data		: data,
				complete	: function() {
					that._$artists.removeClass('is-search');
				},
				success		: function( data ) {
					that._$artists.html('<div class="b-add-item__block b-page__inner b-add-item__albums__inner">' + data + '</div>');
				}
			});
		}

		return false;
	},


	/**
	 * Выбрали артиста
	 * @param artistId
	 */
	chooseArtist: function( artistId ) {
		this.currentArtistId = artistId;
		var $artistInfo = this.$('[data-artist-info]');
		var $albumForm	= this.$('[data-album-search-form]');

		// TODO: вот сюда надо встроить запрос с получением инфы об артисте или генерацию html блока с этой инфой. Сейчас блок просто содержит статичную html верстку
		// Показываем инфу об артисте
		$artistInfo.show();

		// Скриваем блок с запросом артиста и результами поиска
		this.$('[data-artist-search-form], [data-artists]').hide();
		$albumForm.removeClass('no-active');

		// Скролим к артистам
		var artistPosition = $artistInfo.offset().top;
		if ( artistPosition ) {
			this.scrollTo(artistPosition, function() {
				$albumForm.find('[name="name"]').focus();
			});
		}
	},


	/**
	 * Выбор альбома
	 * @param albumId
	 */
	chooseAlbum: function( albumId ) {
		this.currentAlbumId = albumId;
		var $albumInfo = this.$('[data-album-info]');
		var $infoForm	= this.$('[data-item-info-form]');

		// TODO: вот сюда надо встроить запрос с получением инфы об альбоме или генерацию html блока с этой инфой. Сейчас блок просто содержит статичную html верстку
		// Показываем инфу об артисте
		$albumInfo.show();

		// Скриваем блок с запросом артиста и результами поиска
		this.$('[data-album-search-form], [data-albums]').hide();
		$infoForm.removeClass('no-active');

		// Скролим к артистам
		var artistPosition = $albumInfo.offset().top;
		if ( artistPosition ) {
			this.scrollTo(artistPosition, function() {
				$infoForm.find('[name="name"]').focus();
			});
		}
	},


	_e_findAlbums: function( event ) {
		var that		= this;
		var $form		= $(event.currentTarget);
		var $albums		= that.$('[data-albums]');
		var data		= {
			artist_id		: this.currentArtistId,
			album_name		: $form.find('[name="name"]').val()
		};

		$albums.addClass('is-search has-border').html('');
		that.currentAlbumId = null;

		$.ajax({
			url			: $form.attr('action'),
			type		: 'post',
			dataType	: 'html',
			data		: data,
			complete	: function() {
				$albums.removeClass('is-search');
			},
			success		: function( data ) {
				$albums.html('<div class="b-add-item__block b-page__inner b-add-item__albums__inner">' + data + '</div>');
			}
		});

		return false;
	},


	/**
	 *
	 * @param	{Event}	event
	 * @private
	 */
	_e_selectArtist: function( event ) {
	    var $target		= $(event.currentTarget);
		var artistId	= $target.data('item-id');
		this.chooseArtist(artistId);
	},


	/**
	 *
	 * @param	{Event}	event
	 * @private
	 */
	_e_selectAlbum: function( event ) {
	    var $target = $(event.currentTarget);
		var albumId	= $target.data('item-id');
		this.chooseAlbum(albumId);
	},


	_closeArtist: function() {
		if ( this.currentAlbumId ) {
			this._closeAlbum();
			this.$('[data-album-search-form]').find('[name="name"]').val('');
		}
		this.$('[data-albums]').html('').removeClass('has-border');

		this.currentArtistId = null;
		this.$('[data-artist-info]').hide();
		this.$('[data-artist-search-form], [data-artists]').show();
		this.$('[data-album-search-form]').addClass('no-active');
	},


	_closeAlbum: function() {
		this.currentAlbumId = null;
		this.$('[data-album-info]').hide();
		this.$('[data-album-search-form], [data-albums]').show();
		this.$('[data-item-info-form]').addClass('no-active');
	}
});