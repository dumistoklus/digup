<?
$is_edit = false;
?>
<div class="b-content" data-module-name="AddItem">
	<div class="b-page__inner b-add-item__header">
		<h1 class="b-add-item__title">Добавить товар</h1>
		<a href="release" class="b-btn is-blue is-full-colored b-add-item__add-realise">Добавить релиз</a>
		<p>Вы в трех шагах от того, чтобы добавить ваш товар в Маркет. Произведение берется из базы нашей постоянно пополняемой Коллекции со всей необходимой информацией.</p>
		<p>Тем не менее, если в списке вы не нашли своего товара, добавьте релиз самостоятельно, нажав кнопку справа.</p>
		<a href="faq" class="b-add-item__header__link">Нужна помощь?</a>
	</div>
	<?
		include ('common/addOrEditItem.php');
	?>
</div>