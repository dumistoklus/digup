<div class="b-messages__interlocutor">
	<a href="javascript:void(0)" class="b-messages__interlocutor__open-actions-button" data-open-actions><i class="icon-angle-down"></i></a>
	<a href="seller" class="b-messages__interlocutor__avatar">
		<img src="templates/images/_content/avatars/2.jpg" class="b-messages__interlocutor__avatar__img">
	</a>
	<a href="seller" class="b-messages__interlocutor__name">
		Andrew
	</a>
	<div class="b-messages__interlocutor__actions" data-dialog-actions>
		<a href="javascript:void(0)" class="b-messages__interlocutor__actions__option">Заблокировать</a>
		<a href="javascript:void(0)" class="b-messages__interlocutor__actions__option">Отменить сделку</a>
	</div>
</div>
<div class="b-messages__dialog b-scroll__wrapper" data-dialog>
	<div class="b-scroll" data-dialog-scroll>
		<div class="b-scroll__inner" data-messages>
			<div class="b-messages__deal">
				<div class="b-messages__deal__cover">
					<img src="templates/images/_content/items/4.jpg" class="b-messages__deal__cover__img" />
				</div>
				<div class="b-messages__deal__content">
					<div class="b-messages__deal__right-button">
						<a href="deals" class="b-btn is-blue">Сделки</a>
					</div>
					<div class="b-messages__deal__title">Tears In Your Eyes (Over Over-Blackout)</div>
					<div class="b-messages__deal__artist">Hamid Baroudi</div>
					<div class="b-messages__deal__props__left b-messages__deal__props">
						<div class="b-messages__deal__props__dt">T</div>
						<div class="b-messages__deal__props__dd">винил</div>
						<div class="b-messages__deal__props__dt">Q</div>
						<div class="b-messages__deal__props__dd">M</div>
					</div>
					<div class="b-messages__deal__props__right b-messages__deal__props">
						<div class="b-messages__deal__props__dt">количество</div>
						<div class="b-messages__deal__props__dd">1 шт</div>
						<div class="b-messages__deal__props__dt">статус</div>
						<div class="b-messages__deal__props__dd">в ожидании</div>
					</div>
				</div>
			</div>
			<div class="b-messages__message">
				<a href="seller" class="b-messages__message__avatar">
					<img src="templates/images/_content/avatars/2.jpg" class="b-messages__message__avatar__img" />
				</a>
				<a href="seller" class="b-messages__message__name">Виктор</a>
				<div class="b-messages__message__time">14.07.2013</div>
				<div class="b-messages__message__text">
					Здравствуйте, хотел спросить, оригинальная ли обложка в этом экземпляре?
				</div>
			</div>
			<div class="b-messages__message">
				<a href="seller" class="b-messages__message__avatar">
					<img src="templates/images/_content/avatars/1.jpg" class="b-messages__message__avatar__img" />
				</a>
				<a href="seller" class="b-messages__message__name">Обама</a>
				<div class="b-messages__message__time">14.07.2013</div>
				<div class="b-messages__message__text">
					Нет, взял её с другого очень похожего альбома
				</div>
			</div>
			<div class="b-messages__message">
				<a href="seller" class="b-messages__message__avatar">
					<img src="templates/images/_content/avatars/2.jpg" class="b-messages__message__avatar__img" />
				</a>
				<a href="seller" class="b-messages__message__name">Виктор</a>
				<div class="b-messages__message__time">14.07.2013</div>
				<div class="b-messages__message__text">
					Печалька. Ну брать тогда не буду
				</div>
			</div>
			<div class="b-messages__message">
				<a href="seller" class="b-messages__message__avatar">
					<img src="templates/images/_content/avatars/1.jpg" class="b-messages__message__avatar__img" />
				</a>
				<a href="seller" class="b-messages__message__name">Обама</a>
				<div class="b-messages__message__time">14.07.2013</div>
				<div class="b-messages__message__text">
					Да ты бери дорогой! Не пожалеешь!
				</div>
			</div>
			<div class="b-messages__message">
				<a href="seller" class="b-messages__message__avatar">
					<img src="templates/images/_content/avatars/2.jpg" class="b-messages__message__avatar__img" />
				</a>
				<a href="seller" class="b-messages__message__name">Виктор</a>
				<div class="b-messages__message__time">14.07.2013</div>
				<div class="b-messages__message__text">
					Печалька. Ну брать тогда не буду
				</div>
			</div>
			<div class="b-messages__message">
				<a href="seller" class="b-messages__message__avatar">
					<img src="templates/images/_content/avatars/1.jpg" class="b-messages__message__avatar__img" />
				</a>
				<a href="seller" class="b-messages__message__name">Обама</a>
				<div class="b-messages__message__time">14.07.2013</div>
				<div class="b-messages__message__text">
					Да ты бери дорогой! Не пожалеешь!
				</div>
			</div>
		</div>
	</div>
</div>