<?
	$is_main_page = true;
?>
<div class="b-content" data-module-name="Index">
	<div class="b-main-header" style="background-image: url(templates/images/_content/index/header.jpg);">
		<div class="b-main-header__title">Найди свою музыку</div>
		<div class="b-main-header__description">Онлайн-площадка  для покупки, продажи<br />
			и обмена музыкальными носителями</div>
		<div class="b-main-header__search">
			<form action="collection">
				<input type="text" name="q" placeholder="Найти" class="b-main-header__search__field" />
				<button class="b-main-header__search__button"><i class="icon-angle-right"></i></button>
				<input type="submit" class="is-hidden">
			</form>
		</div>
		<a href="item" class="b-main-header__cover-link">
			Classic Gangster
			<div class="b-main-header__cover-link__artist">Jay-Z</div>
		</a>
	</div>
	<div class="b-index">
		<div class="b-index__selector">
			<a href="javascript:void(0)" class="b-index__selector__link is-active" data-history-selector="week">За неделю</a>
			<a href="javascript:void(0)" class="b-index__selector__link" data-history-selector="month">За месяц</a>
		</div>
		<div class="b-index__type">Маркет: новое</div>

		<? // Новое за неделю ?>
		<div class="b-index__collection" data-history="week">
			<div class="b-index__collection__line">
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/1.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Under The Iron Sea</div>
						<div class="b-item__text__hint">Keane</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/4.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Off The Deep End</div>
						<div class="b-item__text__hint">Weird All Yankivic</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/3.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Classic Gangster</div>
						<div class="b-item__text__hint">Jay-Z</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item is-right-border">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/2.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Faces</div>
						<div class="b-item__text__hint">Grave Motion</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
			</div>
			<div class="b-index__collection__line">
				<a href="item" class="b-index__collection__item b-item is-big-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/1.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Under The Iron Sea</div>
						<div class="b-item__text__hint">Keane</div>
					</div>
					<div class="b-item__stat">
						<div class="b-item__stat__right">
							<span class="b-item__button">от 52 $</span>
						</div>
						<span class="b-item__stat__rating">
							<span class="icon-stat"></span> 4.8
						</span>
						<span class="b-item__stat__plus">
							<span class="icon-plus"></span> 15
						</span>
						<span class="b-item__stat__ok">
							<span class="icon-ok"></span> 332
						</span>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Лейбл</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Страна</span>UK</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Дата релиза</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Жанр</span>JAZZ</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Стиль</span>jazzy</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/7.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Funky Town</div>
						<div class="b-item__text__hint">Prince</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item is-right-border">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/5.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Dark Side Of The Moon</div>
						<div class="b-item__text__hint">Pink Floyd</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/3.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Classic Gangster</div>
						<div class="b-item__text__hint">Jay-Z</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item is-right-border">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/4.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Off The Deep End</div>
						<div class="b-item__text__hint">Weird All Yankivic</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
			</div>
		</div>

		<? // Новое за месяц ?>
		<div class="b-index__collection is-hidden" data-history="month">
			<div class="b-index__collection__line">
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/2.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Under The Iron Sea</div>
						<div class="b-item__text__hint">Keane</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/3.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Off The Deep End</div>
						<div class="b-item__text__hint">Weird All Yankivic</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/1.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Classic Gangster</div>
						<div class="b-item__text__hint">Jay-Z</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item is-right-border">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/4.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Faces</div>
						<div class="b-item__text__hint">Grave Motion</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
			</div>
			<div class="b-index__collection__line">
				<a href="item" class="b-index__collection__item b-item is-big-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/1.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Under The Iron Sea</div>
						<div class="b-item__text__hint">Keane</div>
					</div>
					<div class="b-item__stat">
						<div class="b-item__stat__right">
							<span class="b-item__button">от 52 $</span>
						</div>
						<span class="b-item__stat__rating">
							<span class="icon-stat"></span> 4.8
						</span>
						<span class="b-item__stat__plus">
							<span class="icon-plus"></span> 15
						</span>
						<span class="b-item__stat__ok">
							<span class="icon-ok"></span> 332
						</span>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Лейбл</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Страна</span>UK</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Дата релиза</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Жанр</span>JAZZ</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Стиль</span>jazzy</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/7.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Funky Town</div>
						<div class="b-item__text__hint">Prince</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item is-right-border">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/5.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Dark Side Of The Moon</div>
						<div class="b-item__text__hint">Pink Floyd</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/3.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Classic Gangster</div>
						<div class="b-item__text__hint">Jay-Z</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
				<a href="item" class="b-index__collection__item b-item is-right-border">
					<div class="b-item__img" style="background-image: url(templates/images/_content/items/4.jpg)"></div>
					<div class="b-item__text">
						<div class="b-item__text__name">Off The Deep End</div>
						<div class="b-item__text__hint">Weird All Yankivic</div>
					</div>
					<div class="b-item__overlay">
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						<div class="b-item__overlay__button-wrapper">
							<span class="b-item__button">от 35 $</span>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="b-index__blog">
		<div class="b-index__blog__background" data-blog-background></div>
		<div class="b-index__blog__inner">
			<div class="b-index">
				<div class="b-index__type">Блог: последнее</div>
				<div class="b-index__blog__content">
					<div class="b-index__blog__text">
						<a href="blog_post" class="b-index__blog__text__title">Что следует ожидать
						в будущем музыкальной
						индустрии</a>
						Акцепт опровергает незаконный Указ, исключая принцип презумпции невиновности. Правоспособность, в согласии с традиционными представлениями, добросовестно использует нормативный предпринимательский риск, исключая принцип презумпции невиновности
					</div>
					<div class="b-index__blog__posts">
						<div class="b-index__blog__posts__post">
							<div class="b-index__blog__posts__post__avatar">
								<img src="templates/images/_content/avatars/1.jpg">
							</div>
							<div class="b-index__blog__posts__post__text">
								<div class="b-index__blog__posts__post__data">
									14.07.2013
									<span class="icon-views"></span>
									355
									<span class="icon-comments"></span>
									4
								</div>
								<a href="blog_post" class="b-index__blog__posts__post__title">
									20 новых исполнителей,
									которых стоить послушать
								</a>
							</div>
						</div>
						<div class="b-index__blog__posts__post">
							<div class="b-index__blog__posts__post__avatar">
								<img src="templates/images/_content/avatars/2.jpg">
							</div>
							<div class="b-index__blog__posts__post__text">
								<div class="b-index__blog__posts__post__data">
									14.07.2013
									<span class="icon-views"></span>
									34587
									<span class="icon-comments"></span>
									14
								</div>
								<a href="blog_post" class="b-index__blog__posts__post__title">
									«Нудятина про андеграунд к музыке не имеет никакого отношения»
								</a>
							</div>
						</div>
						<div class="b-index__blog__posts__post">
							<div class="b-index__blog__posts__post__avatar">
								<img src="templates/images/_content/avatars/3.jpg">
							</div>
							<div class="b-index__blog__posts__post__text">
								<div class="b-index__blog__posts__post__data">
									15.07.2013
									<span class="icon-views"></span>
									3
									<span class="icon-comments"></span>
									0
								</div>
								<a href="blog_post" class="b-index__blog__posts__post__title">
									Смежные территории Розанна Кэш, Drive-By Truckers, Родни Кроуэлл и другие
								</a>
							</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="b-index__items-list">
		<div class="b-index__items-list__title">Коллекция: популярное</div>
		<div class="b-index__items-list__list">
			<!-- расчинтано на 20 элементов -->
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/2.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/3.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/4.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/5.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/6.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/2.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/4.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/2.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/3.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/4.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/8.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
			<a href="item" class="b-index__items-list__list__item">
				<img src="templates/images/_content/items/small/1.jpg">
			</a>
		</div>
	</div>
</div>