'use strict';

app.Cookie = (function(){

	function Cookie() {
		this.prefix = '';
	}

	Cookie.prototype.setCookie = function(name, value, expires, path, domain, secure) {

		if (!name) {
			return false;
		}
		var str = this.prefix + name + '=' + encodeURIComponent(value);

		if (expires) {
			str += '; expires=' + expires.toGMTString();
		}
		if (path) {
			str += '; path=' + path;
		}
		if (domain) {
			str += '; domain=' + domain;
		}
		if (secure) {
			str += '; secure';
		}
		document.cookie = str;

		return true;
	};

	Cookie.prototype.getCookie = function(name) {

		var pattern = '(?:; )?' + this.prefix + name + '=([^;]*);?',
			regexp  = new RegExp(pattern)
		;

		if (regexp.test(document.cookie)) {
			return decodeURIComponent(RegExp.$1);
		}
		return false;
	};

	Cookie.prototype.deleteCookie = function(name, path, domain) {

		this.setCookie(name, null, new Date(0), path, domain);
	};

	return new Cookie();

})();