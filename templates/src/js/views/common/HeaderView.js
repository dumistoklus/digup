'use strict';

/**
 * @class	app.views.HeaderView
 * @extends Backbone.View
 */
app.views.HeaderView = Backbone.View.extend({

	el					: '.b-header',

	_isRegistrationTab	: true,
	_isAuthOpen			: false,

	isMenuOpen			: false,

	events				: {
		'click [data-toggle-menu]'		: '_e_toggleMenu',
		'keyup [type="search"]'			: '_e_changeInput',
		'change [type="search"]'		: '_e_changeInput',
		'click [data-open-login]'		: '_e_openLogin'
	},


	initialize: function() {
		var that = this;
		app.dom.$document.on('click', function(){
			if ( that.isMenuOpen ) {
				that.closeMenu();
			}
		});

		// Инициализируем первоначальную подсветку поиска
		var $search = this.$('[type="search"]');
		if ( $search.length ) {
			this._e_changeInput({target: $search});
		}

		this._initAuth();
	},


	openMenu: function() {
		this.$('.js-menu').addClass('is-open');
		this.isMenuOpen = true;
	},


	closeMenu: function() {
		this.$('.js-menu').removeClass('is-open');
		this.isMenuOpen = false;
	},


	_e_toggleMenu: function() {
		if ( !this.isMenuOpen ) {
			this.openMenu();
			return false;
		}
	},


	_e_changeInput: function( event ) {
		var $input	= $(event.target);
		var value	= $input.val();
		var $button	= $input.prev();

		if ( value.length ) {
			$button.addClass('is-highlighted');
		} else {
			$button.removeClass('is-highlighted');
		}
	},


	_initAuth: function() {
		var that = this;
		this.$auth = $('[data-auth-popup]');
		this.$auth.on('click', '[data-open-registration]', function() {
			that._e_openRegistration();
		});
		this.$auth.on('click', '[data-open-login]', function() {
			that._e_openLogin();
		});
	},


	_e_openLogin: function() {
		if ( this._isRegistrationTab ) {
			this.$auth.removeClass('is-registration').addClass('is-login');
		}
		if ( !this._isAuthOpen ) {
			this.showAuthPopup();
		}
		this._isRegistrationTab = false;
	},


	_e_openRegistration: function() {
		if ( !this._isRegistrationTab ) {
			this.$auth.addClass('is-registration').removeClass('is-login');
		}
		if ( !this._isAuthOpen ) {
			this.showAuthPopup();
		}
		this._isRegistrationTab = true;
	},


	showAuthPopup: function() {
		var $popup = $('[data-auth-popup]');

		$popup.fadeIn();
		$popup.on('click', function( event ){
			var $target = $(event.target);
			if ( $target.filter('[data-auth-popup]').length ) {
				$target.fadeOut();
			}
		});
	}
});