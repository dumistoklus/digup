'use strict';

/**
 * @class	app.views.Stream
 * @extends app.views.Default
 */
app.views.Stream = app.views.Default.extend({


	events: {
		'click [data-stream-block]' : '_e_openStreamPopup',
		'click [data-more]' 		: '_e_more'
	},


	_page: 0,


	_isLoad: false,


	initialize: function() {
		this._url = this.$el.data('url');
	},


	_e_openStreamPopup: function( event ) {
	    var $target 	= $(event.currentTarget),
			data		= $target.data('items'),
			itemsHtml	= '',
			html,
			$popup
		;

		html = '<div class="b-popup__header">\
					<div class="b-popup__header__avatar">\
						<img src="' + data.avatar + '" />\
					</div>\
					<div class="b-popup__header__link">\
						<a href="javascript:void(0)" class="b-btn is-blue">Перейти к профилю</a>\
					</div>\
					<div class="b-popup__header__content">\
						<div class="b-popup__header__name">' + data.name + '</div>\
						<div class="b-popup__header__date">' + data.hint + ', '  + data.time + '</div>\
					</div>\
				</div>'
		;

		if ( data ) {
			$.each(data.items, function(key, item) {
				itemsHtml += '<a href="' + item.url + '" class="b-popup__item b-item">\
					<div class="b-item__img" style="background-image: url(' + item.image + ')"></div>\
					<div class="b-item__text">\
						<div class="b-item__text__name">' + item.name + '</div>\
						<div class="b-item__text__hint">' + item.artist + '</div>\
					</div>\
					<div class="b-item__overlay">\
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>' + item.artist + '</div>\
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>' + item.label + '</div>\
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>' + item.year + '</div>\
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>' + item.type + '</div>\
						<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>' + item.quality + '</div>\
						<div class="b-item__overlay__button-wrapper">\
							<span class="b-item__button">' + item.price + ' $</span>\
						</div>\
					</div>\
				</a>';
			});
		}

		html = '<div class="b-popup__overlay" data-popup><div class="b-popup">' + html + '<div class="b-popup__items">' + itemsHtml + '</div></div></div>';

		// Формируем попап
		$popup = $(html);

		// Вставляем попап в дом
		app.dom.$body.append($popup);

		// Показываем попап
		this.showPopup($popup);
	},


	_e_more: function() {
		var that = this;

		if ( this._isLoad ) {
			return;
		}

		this._isLoad = true;
		this._page++;

		this.$el.addClass('is-loading');

		$.ajax({
			url: this._url + '?page=' + this._page,
			dataType: 'html',
			success: complete,
			complete: function() {
				that.$el.removeClass('is-loading');
				that._isLoad = false;
			}
		});

		// Обрабаотывает ответ сервера, содержащий всю страницу
		function complete( data ) {

			// Выбираем участок для создания dom документа, чтобы исключить пападания туда скриптов
			// Если засунуть все это в jQuery, то все скрипты выполнятся второй раз
			var targetContent = /<!-- ITEMS -->([\S\s]*)<!-- \/ ITEMS -->/.exec( data );

			// Делаем редирект, если пришла не стандартная страница
			if ( targetContent && targetContent[1] ) {
				that._insertBlocks($(targetContent[1]));
			}
		}
	},


	_insertBlocks: function( $page ) {
		var $half = this.$('[data-half]');
		var $blocks = $page.find('[data-stream-block]');
		var $fakeDom = $('<div style="position: absolute; left: 100%; top: 0;" />');

		$fakeDom.append($page);

		function insertBlock( $block ) {

			var firstHeight = $half.eq(0).height();
			var secondHeight = $half.eq(1).height();
			var $container;

			if ( firstHeight < secondHeight ) {
				$container = $half.eq(0);
			} else {
				$container = $half.eq(1);
			}

			$container.append($block);
		}

		$blocks.each(function() {
		   insertBlock($(this));
		});

		$fakeDom.remove();
	}
});