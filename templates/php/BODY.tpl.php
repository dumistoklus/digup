<!DOCTYPE html>
<html lang="ru" class="mobile">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=1000, maximum-scale=1.0">

	<title>Digup.com</title>
	<link rel="stylesheet" href="templates/build/styles.min.css"/>
</head>

<body>
<div class="b-notifications">
	<div class="b-notifications__inner">
		<div class="b-notifications__list" data-notifications></div>
	</div>
</div>
<div class="b-auth__overlay" data-auth-popup>
	<div class="b-auth">
		<div class="b-auth__tabs">
			<a href="javascript:void(0)" class="b-auth__tabs__link" data-open-registration>Регистрация</a>
			<a href="javascript:void(0)" class="b-auth__tabs__link" data-open-login>Вход</a>
		</div>
		<div class="b-auth__form b-auth__form-login">
			<form method="post">
				<label class="b-auth__form__label">
					<input name="email" type="text" class="b-auth__form__input" placeholder="Email или никнейм" required>
				</label>
				<label class="b-auth__form__label">
					<input name="password" type="password" class="b-auth__form__input" placeholder="Пароль" required>
				</label>
				<div class="b-auth__form__button__container">
					<button type="submit" class="b-btn is-blue is-full-colored b-auth__form__button">&nbsp; &nbsp; Войти &nbsp; &nbsp; </button>
				</div>
			</form>
		</div>
		<div class="b-auth__form b-auth__form-registration">
			<form method="post">
				<label class="b-auth__form__label">
					<input name="name" type="text" class="b-auth__form__input" placeholder="Никнейм" required>
				</label>
				<label class="b-auth__form__label">
					<input name="email" type="text" class="b-auth__form__input" placeholder="Email" required>
				</label>
				<label class="b-auth__form__label">
					<input name="password" type="password" class="b-auth__form__input" placeholder="Пароль" required>
				</label>
				<label class="b-auth__form__label">
					<input name="password" type="password" class="b-auth__form__input" placeholder="Повторите пароль" required>
				</label>
				<div class="b-auth__form__button__container">
					<button type="submit" class="b-btn is-blue is-full-colored b-auth__form__button is-right">&nbsp; &nbsp; Отправить &nbsp; &nbsp; </button>
					<div class="b-auth__form__button__text">
						Подтверждая регистрацию,<br />
						вы соглашаетесь с <a href="">условиями сервиса</a>.
					</div>
				</div>
			</form>
		</div>
		<div class="b-auth__social">
			<div class="b-auth__social__title">
				Войти через соц. сети
			</div>
			<a href="#" class="b-auth__social__link"><i class="icon-vk"></i></a><!--
			--><a href="#" class="b-auth__social__link"><i class="icon-facebook"></i></a><!--
			--><a href="#" class="b-auth__social__link"><i class="icon-twitter"></i></a>
		</div>
	</div>
</div>
<div class="b-main-container">
	<div class="b-header <?= isset( $is_main_page ) ? ' is-main-page' : '' ?>">
		<div class="b-header__inner">
			<div class="b-header__menu js-menu">
				<?
				// Авторизован
				if ( mt_rand(0,1)  > 0.5 ) {
					?>
					<a href="javascript:void(0)" class="b-header__menu__link" data-toggle-menu>
						<span class="b-header__menu__link__menu-button">
							<span class="icon-menu"></span>
						</span>
						<img src="templates/images/_content/avatar.jpg" class="b-header__menu__link__menu-button__img" /><!--
						--><span class="b-header__menu__link__text">ILYA GOLDFARB</span>
					</a>
					<div class="b-header__menu__container">
						<a href="cart" class="b-header__menu__container__link">Корзина<span class="b-header__menu__container__link__icon">5</span></a>
						<a href="deals" class="b-header__menu__container__link">Мои сделки</a>
						<a href="messages" class="b-header__menu__container__link">Сообщения<span class="b-header__menu__container__link__icon">5</span></a>
						<a href="#" class="b-header__menu__container__link">Закладки</a>
						<a href="#" class="b-header__menu__container__link">Статистика</a>
						<a href="settings" class="b-header__menu__container__link">Настройки</a>
						<a href="#" class="b-header__menu__container__link">Выйти</a>
						<a href="add" class="b-header__menu__container__link is-blue">Добавить товар <span class="b-header__menu__container__link__icon"><span class="icon-plus"></span></span></a>
					</div>
				<?
				}

				// Не авторизован
				else {
					?>
					<a href="javascript:void(0)" class="b-header__menu__link" data-open-login>
						<span class="b-header__menu__link__menu-button">
							<span class="icon-arrow-right"></span>
						</span>
						<span class="b-header__menu__link__login-icon">
							<span class="icon-login"></span>
						</span><!--
						--><span class="b-header__menu__link__text">ВОЙТИ</span>
					</a>
					<?
				}
				?>
			</div>
			<?
				if ( isset( $is_main_page ) ) {
					include('pages/common/main-menu.php');
				} else {
					?>
					<div class="b-header__search">
						<form action="" method="get">
							<button type="submit"><span class="icon-search"></span></button>
							<input type="search" name="query" placeholder="Найти товар" />
							<input type="submit" />
						</form>
					</div>
					<?
				}
			?>
		</div>
	</div>
	<?
	if ( !isset( $is_main_page ) ) {
		include('pages/common/main-menu.php');
	}
	?>

	<?= $CONTENT ?>

	<div class="b-footer">
		<div class="b-footer__inner">
			<div class="b-footer__social">
				<a href="http://vk.com" class="b-footer__social__link"><span class="icon-vk"></span></a>
				<a href="http://twitter.com" class="b-footer__social__link"><span class="icon-twitter"></span></a>
				<a href="http://fb.com" class="b-footer__social__link"><span class="icon-facebook"></span></a>
			</div>
			<div class="b-footer__links">
				<a href="" class="b-footer__links__link">Раскопки</a>
				<a href="" class="b-footer__links__link">Вопросы-ответы</a>
				<a href="" class="b-footer__links__link">Блог</a>
				<a href="" class="b-footer__links__link">О проекте</a>
				<a href="" class="b-footer__links__link">Пользовательское соглашение</a>
				<a href="" class="b-footer__links__link">Контакты</a>
			</div>
			<div class="b-footer__text">
			2014 © Digup.ru. Интернет-платформа для размещения на продажу или обмен музыкальных товаров.<br />
			Все изображения и музыкальный контент принадлежат их авторам.
			</div>
		</div>
	</div>
</div>
<script>
	/*
	window.messages = [{
		text: 'Вам сообщение от',
		user: 'ladyinko',
		url: 'messages'
	}];


	window.notifications = ['Товар добавлен в корзину'];
	*/
</script>

<? if ( DEBUG ) { ?>
<script src="templates/src/js/libs/jquery-2.1.1.min.js"></script>
<script src="templates/src/js/libs/underscore-1.6.0.min.js"></script>
<script src="templates/src/js/libs/backbone-1.1.2.min.js"></script>
<script src="templates/src/js/libs/modernizr.2.8.2.custom.min.js"></script>
<script src="templates/src/js/libs/jquery.ikSelect.min.js"></script>
<script src="templates/src/js/libs/checkbox.min.js"></script>
<script src="templates/src/js/libs/jquery-ui.min.js"></script>
<script src="templates/src/js/libs/norilskScroll.js"></script>

<script src="templates/src/js/app.js"></script>

<script src="templates/src/js/helpers/Size.js"></script>
<script src="templates/src/js/helpers/Cookie.js"></script>
<script src="templates/src/js/helpers/utils.js"></script>
<script src="templates/src/js/helpers/Notifications.js"></script>

<script src="templates/src/js/views/common/DefaultView.js"></script>
<script src="templates/src/js/views/common/HeaderView.js"></script>
<script src="templates/src/js/views/common/Form.js"></script>

<script src="templates/src/js/views/Index.js"></script>
<script src="templates/src/js/views/Market.js"></script>
<script src="templates/src/js/views/Item.js"></script>
<script src="templates/src/js/views/Shop.js"></script>
<script src="templates/src/js/views/Cart.js"></script>
<script src="templates/src/js/views/Stream.js"></script>
<script src="templates/src/js/views/AddItem.js"></script>
<script src="templates/src/js/views/EditItem.js"></script>
<script src="templates/src/js/views/Messages.js"></script>
<script src="templates/src/js/views/Faq.js"></script>
<script src="templates/src/js/views/Deals.js"></script>
<script src="templates/src/js/views/Seller.js"></script>
<script src="templates/src/js/views/Settings.js"></script>
<script src="templates/src/js/views/AddRelease.js"></script>

<script src="templates/src/js/main.js"></script>
<? } else { ?>
<script type="text/javascript" src="templates/build/base.min.js"></script>
<script type="text/javascript" src="templates/build/script.min.js"></script>
<? } ?>
</body>
</html>