<div class="b-content" data-module-name="Cart">
	<div class="b-page__main-title">Корзина</div>
	<div class="b-page__line">
		<div class="b-page__inner">
			<a href="cart" class="b-page__line__link">Выбор продавца</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<a href="cart2" class="b-page__line__link">Оформление</a>
			<i class="icon-long-arrow-left is-blue"></i>
			<span class="b-page__line__link">Личные данные</span>
			<i class="icon-long-arrow-left"></i>
			<span class="b-page__line__link">Подтверждение</span>
		</div>
	</div>
	<div class="b-page__inner">
		<form action="cart3" method="post">
			<div class="b-big-item-list">
				<div class="b-big-item-list__el" data-item>
					<div class="b-big-item-list__el__checkbox">
						<label><input type="checkbox" name="item[1423]" checked data-item-checkbox></label>
					</div>
					<div class="b-big-item-list__el__cover">
						<a href="item"><img src="templates/images/_content/items/2.jpg" class="b-big-item-list__el__cover__img" /></a>
					</div>
					<div class="b-big-item-list__el__name-and-ratings">
						<div class="b-big-item-list__el__title"><a href="item" class="b-big-item-list__el__title__link">Tightropewalker + In Like Satin</a></div>
						<div class="b-big-item-list__el__artist">Hamid Baroudi</div>
						<div class="b-big-item-list__el__bottom">
							<a href="seller" class="b-big-item-list__el__user"><i class="icon-user"></i> andrew</a>
						</div>
					</div>
					<div class="b-item-list__el__info">
						<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
						<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>M</div>
					</div>
					<a href="javascript:void(0)" class="b-big-item-list__el__close" data-delete-item><i class="icon-delete"></i></a>
					<div class="b-big-item-list__el__price-and-count">
						<span class="b-big-item-list__el__price" data-price="89">
							89 $
						</span>
						1 шт.
						<div class="b-big-item-list__el__bottom">
							<a href="javascript:void(0)" class="b-btn is-block is-blue"><i class="icon-plus"></i> Сообщение</a>
						</div>
					</div>
				</div>
				<div class="b-big-item-list__el" data-item>
					<div class="b-big-item-list__el__checkbox">
						<label><input type="checkbox" name="item[1423]" checked data-item-checkbox></label>
					</div>
					<div class="b-big-item-list__el__cover">
						<a href="item"><img src="templates/images/_content/items/3.jpg" class="b-big-item-list__el__cover__img" /></a>
					</div>
					<div class="b-big-item-list__el__name-and-ratings">
						<div class="b-big-item-list__el__title"><a href="item" class="b-big-item-list__el__title__link">Heart Of Sun / Feel Of Eternity And Even Fufther</a></div>
						<div class="b-big-item-list__el__artist">Hamid Baroudi</div>
						<div class="b-big-item-list__el__bottom">
							<a href="seller" class="b-big-item-list__el__user"><i class="icon-user"></i> fred2125</a>
						</div>
					</div>
					<div class="b-item-list__el__info">
						<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
						<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>G</div>
					</div>
					<a href="javascript:void(0)" class="b-big-item-list__el__close" data-delete-item><i class="icon-delete"></i></a>
					<div class="b-big-item-list__el__price-and-count">
						<span class="b-big-item-list__el__price" data-price="1800">
							1 800 $
						</span>
						1 шт.
						<div class="b-big-item-list__el__bottom">
							<a href="javascript:void(0)" class="b-btn is-block is-blue"><i class="icon-plus"></i> Сообщение</a>
						</div>
					</div>
				</div>
			</div>
			<div class="b-cart__sum">
				<a href="collection" class="b-cart__sum__link">Продолжить раскопки</a>
				<a href="javascript:void(0)" class="b-btn is-blue is-full-colored" data-submit-form><span class="b-btn-part"><span data-full-price>300</span> $</span>Оформить</a>
			</div>
		</form>
	</div>
</div>