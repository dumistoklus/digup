<div class="b-content" data-module-name="Item">
	<div class="b-page__header" data-item-header>
		<div class="b-page__inner">
			<div class="b-page__header__stat-and-buttons">
				<div class="b-page__header__stat-and-buttons__stat">
					<span class="b-item__stat__rating">
						<span class="b-item__stat__icon">
							<span class="b-item__stat__icon__rating"></span><!--
							--><span class="b-item__stat__icon__rating"></span><!--
							--><span class="b-item__stat__icon__rating"></span><!--
							--><span class="b-item__stat__icon__rating"></span>
						</span>
						4.8
					</span>
					<span class="b-item__stat__plus">
						<span class="b-item__stat__icon">
							<span class="b-item__stat__icon__plus"></span>
							<span class="b-item__stat__icon__plus"></span>
						</span>
						<span data-wish-list-count>15</span>
					</span>
					<span class="b-item__stat__ok">
						<span class="b-item__stat__icon">
							<span class="icon-ok"></span>
						</span>
						<span data-owner-list-count>332</span>
					</span>
				</div>
				<div class="b-page__header__stat-and-buttons__buttons">
					<div class="b-page__header__hint">В продаже:</div>
					<div class="b-page__header__types">винил, MP3, WAV, CD, DVD</div>
					<div class="btn-group">
						<a href="javascript:void(0)" class="b-btn is-green">44–52 $</a>
						<a href="javascript:void(0)" class="b-btn is-green">Продавцы</a>
					</div>
				</div>
			</div>
			<div class="b-page__header__cover" data-item-header-cover>
				<a href="templates/images/_content/items/6.jpg" class="b-page__header__cover__inner" data-item-header-cover-inner data-show-full-image>
					<div class="b-page__header__cover__inner__link" data-add-in-watch-list>
						<div class="b-page__header__cover__inner__link__inner"><i class="icon-plus"></i></div>
						<div class="b-page__header__cover__inner__link__text">В Вишлист</div>
					</div>
					<div class="b-page__header__cover__inner__link" data-add-in-owner-list>
						<div class="b-page__header__cover__inner__link__inner"><i class="icon-ok"></i></div>
						<div class="b-page__header__cover__inner__link__text">Уже есть</div>
					</div>
					<i class="icon-magnifier"></i>
				</a>
				<img src="templates/images/_content/items/1.jpg" class="b-page__header__cover__img">
			</div>
			<div class="b-page__header__main">
				<h1 class="b-page__header__title">
					Heart Of Sun / Feel Of Eternity And Even Further
				</h1>
				<div class="b-page__header__subtitle">
					Kamil Polner
				</div>
				<div class="b-page__params">
					<div class="b-page__params__dt">Лейбл</div>
					<div class="b-page__params__dd">Jazzy Rec</div>
					<div class="b-page__params__dt">Страна</div>
					<div class="b-page__params__dd">UK</div>
					<div class="b-page__params__dt">Дата релиза</div>
					<div class="b-page__params__dd">03.12.1989</div>
					<div class="b-page__params__dt">Жанр</div>
					<div class="b-page__params__dd">Jazz</div>
					<div class="b-page__params__dt">Стиль</div>
					<div class="b-page__params__dd">Jazzy</div>
				</div>
			</div>
		</div>
	</div>
	<div class="b-page__inner">
		<div class="b-page__content">
			<div class="b-page__title">Треклист</div>
			<?
				$trackList = array(
					array(
						'title'		=> 'Wanna Let You Know (Club Mix)',
						'artist'	=> 'Man Of Goodwill',
						'time'		=> '5:49',
						'url'		=> 'music/Man_Of_Goodwill_Wanna_Let_You_Know.mp3'
					),
					array(
						'title'		=> 'Seaside',
						'artist'	=> 'Cassara',
						'time'		=> '6:12',
						'url'		=> 'music/Cassara_Seaside.mp3'
					),
					array(
						'title'		=> 'This Is Not Meth',
						'artist'	=> 'Flex Cop',
						'time'		=> '6:12',
						'url'		=> 'music/Flex_Cop_This_Is_Not_Meth.mp3'
					),
					array(
						'title'		=> 'Like You',
						'artist'	=> 'Oliver Schories',
						'time'		=> '6:12',
						'url'		=> 'music/Oliver_Schories_Like_You.mp3'
					)
				);
			?>
			<div class="b-tracks">
				<? foreach ( $trackList as $key => $track ) {
					?>
						<a href="javascript:void(0)" class="b-tracks__track" data-url="<?= $track['url'] ?>" data-player>
							<div class="b-tracks__track__num"><?= $key + 1 ?></div>
							<div class="b-tracks__track__artist"><?= $track['artist'] ?></div>
							<div class="b-tracks__track__title"><?= $track['title'] ?></div>
							<div class="b-tracks__track__time"><?= $track['time'] ?></div>
							<div class="b-tracks__track__play"><span class="b-tracks__track__play__circle"><i class="icon-play"></i><i class="icon-pause"></i></span></div>
						</a>
					<?
				}
				?>
			</div>
			<div class="b-page__title">Похожие исполнители</div>
			<div class="b-item-short-list">
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/2.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/4.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Dark Side Of The Mushroom</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/5.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
				<a href="item" class="b-item-short-list__el">
					<img src="templates/images/_content/items/7.jpg" class="b-item-short-list__el__img" />
					<div class="b-item-short-list__el__title">Funky Town</div>
					<div class="b-item-short-list__el__artist">Prince</div>
				</a>
			</div>
		</div>
		<div class="b-page__info">
			<div class="b-page__title">О музыканте</div>
			<img src="templates/images/_content/band/1.jpg" class="b-page__info__img">
			<div class="b-page__info__block">Is an American rapper who performs under the name Talib Kweli. His first name, Talib, in Arabic means "student" or "seeker"; his middle name in Swahili means "true". Kweli earned recognition through his work in Black Star, a collaboration with fellow MC Mos Def</div>
			<div class="b-page__info__block">
				<div class="b-page__info__link__block">
					<a href="" class="b-page__info__link"><i class="icon-arrow-right"></i> beatport</a>
				</div>
				<div class="b-page__info__link__block">
					<a href="" class="b-page__info__link"><i class="icon-arrow-right"></i> wikipedia</a>
				</div>
			</div>
			<div class="b-page__info__block-without-padding">
				<div class="b-page__info__title">Поделиться страницей</div>
				<div class="b-page__info__social">
					<a href="https://twitter.com/share" class="twitter-share-button" data-related="jasoncosta" data-lang="en" data-count="horizontal" data-url="http://feldvolk.com/">Tweet</a>
				</div>
				<div class="b-page__info__social">
					<div class="fb-like" data-send="false" data-href="https://www.facebook.com/feldvolk" data-layout="button_count" data-width="150" data-show-faces="false"></div>
				</div>
			</div>
		</div>
	</div>
</div>