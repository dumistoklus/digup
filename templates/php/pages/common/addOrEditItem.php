<div class="b-add-item__block is-grey" data-artist-search-form <?= $is_edit ? 'style="display: none"' : ''?>>
	<div class="b-page__inner">
		<h2 class="b-add-item__block__title">Исполнитель</h2>
		<div class="b-add-item__name-form">
			<form action="searchArtist.php" data-artists-form>
				<input type="submit" class="is-hidden" >
				<div class="b-form-field">
					<input type="text" name="name" data-artist-input>
					<a href="javascript:void(0)" class="b-form__field-submit" data-form-submit><i class="icon-angle-right"></i></a>
				</div>
			</form>
		</div>
	</div>
</div>
<?
// в data-artist-info в редактировании надо вставить ID артиста, при создании можно ничего не вставлять или 0
?>
<div class="b-add-item__block is-hidden is-grey" data-artist-info="<?= $is_edit ? 13242 : 0 ?>" <?= $is_edit ? 'style="display: block"' : ''?>>
	<div class="b-page__inner b-add-item__info">
		<a href="javascript:void(0)" class="b-add-item__info__delete" data-close-artist><i class="icon-delete"></i></a>
		<div class="b-add-item__info__left">
			<h2 class="b-add-item__block__title is-grey">Исполнитель</h2>
			<img src="templates/images/_content/artists/1.jpg" class="b-add-item__info__img" />
		</div>
		<div class="b-add-item__info__right">
			<h2 class="b-add-item__block__title is-black">Weird All Yankovic</h2>
			<div class="b-add-item__info__description">
				Is an American rapper who performs under the name Talib Kweli. His first name, Talib, in Arabic means "student" or "seeker"; his middle name in Swahili means "true". Kweli earned recognition through his work in Black Star, a collaboration with fellow MC Mos Def.
			</div>
			<div class="b-add-item__info__links">
				<div>
					<a href="http://beatport.com" class="b-add-item__info__links__link"><i class="icon-arrow-right"></i> beatport</a>
				</div>
				<div>
					<a href="http://wikipedia.com" class="b-add-item__info__links__link"><i class="icon-arrow-right"></i> wikipedia</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="b-add-item__search-block b-add-item__artists" data-artists></div>
<div class="b-add-item__block is-grey no-active" data-album-search-form <?= $is_edit ? 'style="display: none"' : ''?>>
	<div class="b-page__inner">
		<h2 class="b-add-item__block__title">Название альбома или композиции</h2>
		<div class="b-add-item__name-form">
			<form action="searchAlbum.php" data-albums-form>
				<input type="submit" class="is-hidden" >
				<div class="b-form-field">
					<input type="text" name="name" data-artist-input>
					<a href="javascript:void(0)" class="b-form__field-submit" data-form-submit><i class="icon-angle-right"></i></a>
				</div>
			</form>
		</div>
	</div>
</div>
<?
// в data-album-info в редактировании надо вставить ID альбома, при создании можно ничего не вставлять или 0
?>
<div class="b-add-item__block is-hidden is-grey" data-album-info="<?= $is_edit ? 412354 : '' ?>" <?= $is_edit ? 'style="display: block"' : ''?>>
	<div class="b-page__inner b-add-item__info">
		<a href="javascript:void(0)" class="b-add-item__info__delete" data-close-album><i class="icon-delete"></i></a>
		<h2 class="b-add-item__block__title is-grey">Название альбома или композиции</h2>
		<div class="b-add-item__info__left">
			<img src="templates/images/_content/items/1.jpg" class="b-add-item__info__img" />
		</div>
		<div class="b-add-item__info__center">
			<h2 class="b-add-item__block__title is-black">Weird All Yankovic</h2>
		</div>
		<div class="b-add-item__info__right b-page__params">
			<div class="b-page__params__dt">Лейбл</div>
			<div class="b-page__params__dd">Jazzy Rec</div>
			<div class="b-page__params__dt">Страна</div>
			<div class="b-page__params__dd">UK</div>
			<div class="b-page__params__dt">Дата релиза</div>
			<div class="b-page__params__dd">03.12.1989</div>
			<div class="b-page__params__dt">Жанр</div>
			<div class="b-page__params__dd">Jazz</div>
			<div class="b-page__params__dt">Стиль</div>
			<div class="b-page__params__dd">Jazzy</div>
		</div>
	</div>
</div>
<div class="b-add-item__search-block b-add-item__albums" data-albums></div>
<div class="b-add-item__block is-grey <?= $is_edit ? '' : ' no-active'?>" data-item-info-form>
	<div class="b-page__inner">
		<form method="post">
			<h2 class="b-add-item__block__title">Персональные данные товара</h2>
			<div class="b-add-item__name-form">
				<div class="b-add-item__info-form">
					<div class="b-add-item__info__left">
						<div class="b-form-field">
							<select name="order">
								<option value="">Формат носителя</option>
								<option value="1">CD</option>
								<option value="2">Vinyl</option>
							</select>
						</div>
						<div class="b-form-field">
							<div class="b-add-item__info__currency-input">
								<input type="text" name="price" placeholder="Цена заказа">
							</div><span class="b-add-item__info__currency">USD</span>
						</div>
						<div class="b-form-field">
							<select name="order">
								<option value="">Способ доставки</option>
								<option value="1">Без доставки</option>
								<option value="2">Почтой России</option>
							</select>
						</div>
					</div>
					<div class="b-add-item__info__left">
						<div class="b-form-field">
							<select name="order">
								<option value="">Состояние товара</option>
								<option value="1">Хорошее</option>
								<option value="2">Среднее</option>
								<option value="3">Плохое</option>
							</select>
						</div>
						<div class="b-form-field">
							<select name="order">
								<option value="">Способы оплаты</option>
								<option value="1">На сайте</option>
								<option value="2">Никак</option>
							</select>
						</div>
						<div class="b-form-field">
							<div class="b-add-item__info__currency-input">
								<input type="text" name="price" placeholder="Цена доставки">
							</div><span class="b-add-item__info__currency">USD</span>
						</div>
					</div>
					<div class="b-add-item__info__right">
						<div class="b-form-field">
							<textarea name="comment" placeholder="Комментарии к заказу" class="b-add-item__info__textarea"></textarea>
						</div>
					</div>
				</div>
				<div class="b-cart-form__text">
					Заполните все данные. Добавляя товар, вы соглашаетесь с <a href="">условиями сервиса</a>.
				</div>
				<div class="b-cart__sum">
					<a href="add" class="b-cart__sum__link">Отменить</a>
					<button type="submit" class="b-btn is-blue is-full-colored">&nbsp; &nbsp; <?= $is_edit ? 'Сохранить' : 'Добавить товар' ?> &nbsp; &nbsp;</button>
				</div>
			</div>
		</form>
	</div>
</div>