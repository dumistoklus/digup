<?
$query = strtolower($_REQUEST['term']);

$names = array(
	'Madonna',
	'Rolling Stones',
	'Lady Gaga',
	'Black Eyed piece',
	'More pain',
	'Enya',
	'Manowar',
	'Slipknot',
	'Robbie Williams',
	'Bob Dylan',
	'Led Zeppelin',
	'Queen',
	'Smokey Robinson and The Miracles',
	'Diana Ross and the Supremes',
	'Seth MacFarlane',
	'Irving Berlin',
	'Sammy Davis, Jr.',
	'Afghan Whigs',
	'Lilly Wood & The Prick',
	'Låpsley',
);

$result = array();

foreach ( $names as $key => $item ) {
	if ( strpos(strtolower($item), $query) !== false ) {
		$result[] = array(
			'label' => $item,
			'value' => $key
		);
	}
}


$result = array_slice($result, 0, 10);

header('Content-type: application/json');
echo json_encode($result);