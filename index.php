<?php

define('ROOT_DIR',	realpath( __DIR__ ) . '/' );
define('WWW_DIR',	str_replace('\\', '/', realpath(dirname(__FILE__)) . '/') );
define('WWW_PATH',	str_replace(str_replace('\\', '/', realpath($_SERVER['DOCUMENT_ROOT'])), '', WWW_DIR));
define('DEBUG',		!!file_exists('debug.php'));

$request	= isset( $_GET['REQUEST_URI'] ) ? $_GET['REQUEST_URI'] : '';
$request	= explode( '/', $request );
$module		= $request[0];

if ( $module === '' ) {
	$module = 'index';
}

ob_start();
require_once 'templates/php/pages/' . $module. '.php';
$CONTENT = ob_get_contents();
ob_end_clean();

require_once "templates/php/BODY.tpl.php";