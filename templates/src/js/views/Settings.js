'use strict';

/**
 * @class	app.views.Settings
 * @extends app.views.Form
 */
app.views.Settings = app.views.Form.extend({

	events: {
		'click [data-select-tab]'		: '_e_selectTab',
		'submit form'					: '_e_submitForm',
		'change input[type=file]'		: '_e_addedFileInInput',
		'click [data-change-password]'	: '_e_changePassword'
	},


	initialize: function() {
		app.views.Settings.__super__.initialize.apply(this, arguments);
		this.$avatar = this.$('[data-avatar-image]');
	},


	/**
	 * Парсинг ответа успешной или не успешной загрузки аватарки
	 * @param	{Object}	response
	 * @protected
	 */
	_parseFileUploadResponse: function( response ) {

		// Все нормально загрузилось
		if ( response.user && response.user['^avatar'] ) {
			this.$avatar.attr('src', response.user['^avatar'] );
			this.$el.addClass('is-avatar-loaded');
		}

		// Обработчик ошибок
		else if ( response.errors && response.errors.length > 0 ) {
			_.each(response.errors, function(error){
				app.utils.showError(error);
			});
		}
	},


	_e_addedFileInInput: function( event ) {
		var that = this;
		var address = $(event.currentTarget).data('upload-file-url');

		$.each(event.target.files, function(i, v) {
			var fr	= new FileReader();
			fr.file	= v;
			fr.readAsDataURL(v);

			// Загружаем файл
			that._uploadFile(fr.file, address);
		});
	},


	_e_changePassword: function( event ) {
		var $oldPassword	= this.$('[data-old-password]');
		var $newPassword	= this.$('[data-new-password]');
		var $repeatPassword	= this.$('[data-repeat-password]');
		var oldPassword		= $oldPassword.val();
		var newPassword		= $newPassword.val();
		var repeatPassword	= $repeatPassword.val();
		var $button			= $(event.currentTarget);

		if ( !oldPassword.length ) {
			$oldPassword.parent().addClass('has-error');
			$oldPassword.on('change.error click.error', function(){
				$oldPassword.off('change.error click.error');
				$oldPassword.parent().removeClass('has-error');
			});
		} else if ( !newPassword.length ) {
			app.utils.showError( $button.data('message-no-repeat') );
		} else if ( newPassword !== repeatPassword ) {
			app.utils.showError( $button.data('message-passwords-not-match') );
		} else {
			$.ajax({
				url: $button.data('url'),
				method: 'post',
				dataType: 'json',
				success: function( data ) {
					var i;
					if ( data.errors ) {
						for ( i in data.errors ) {
							app.utils.showError(data.errors[i]);
						}
					} else {
						app.utils.showNotification($button.data('message-changed'));
						$oldPassword.val('');
						$newPassword.val('');
						$repeatPassword.val('');
					}
				}
			});
		}
	}
});