"use strict";

$.fn.scrollAid = function( settings ) {

	function ScrollAid( $element, settings ) {
		var scrollHeight = 1;
		var moveLineLength = 0;
		var innerMoveLength = 0;
		var position = 0;
		var options = {};
		var hasScroll = false;
		var lastScrollPos = 0;
		var startScroll = 0;
		var $scroll;
		var scrollClassName = settings.scrollClassName || 'b-scroll__scroll';

		settings = settings || {};

		function onScroll() {
			var scrollPosition = $element.scrollTop();
			position = scrollPosition / innerMoveLength;
			setPosition();
		}

		function createScroll() {
			var startPosition = 0;
			var isMove = false;
			$scroll = $('<div class="' + scrollClassName + '" />');
			$element.after($scroll);

			$scroll.on('mousedown', function(event){
				isMove = true;
				startPosition = event.pageY;
				startScroll = lastScrollPos;
				app.dom.$body.addClass('is-scroll');
				$scroll.addClass('is-active');
				app.dom.$document.on('mouseup.scrollAid', function(){
					app.dom.$document.off('.scrollAid');
					app.dom.$body.removeClass('is-scroll');
					$scroll.removeClass('is-active');
					isMove = false;
					return false;
				});
				app.dom.$document.on('mousemove.scrollAid', onMouseMove);
			});

			function onMouseMove( event ) {
				if ( isMove ) {
					var diff = event.pageY - startPosition;
					position = (startScroll + diff) / (options.parentHeight - scrollHeight);

					if ( position > 1 ) {
						position = 1;
					}
					if ( position < 0) {
						position = 0;
					}
					$element.scrollTop(position * innerMoveLength);
				}
			}
		}

		function showScroll() {
			hasScroll = true;
			$scroll.css('display', 'block');
		}

		function hideScroll() {
			hasScroll = false;
			$scroll.css('display', 'none');
		}

		function setPosition() {
			lastScrollPos = moveLineLength * position;
			$scroll[0].style[Modernizr.transformPrefix] = 'translate3D(0, ' + lastScrollPos + 'px, 0)';
		}

		function resize( settings ) {
			if ( !settings.parentHeight ) {
				options.parentHeight = $element.height();
			} else {
				options.parentHeight = settings.parentHeight;
			}

			if ( !settings.innerHeight ) {
				options.innerHeight = 0;
				$element.children().each(function() {
					options.innerHeight += $(this).height();
				});
			} else {
				options.innerHeight = settings.innerHeight;
			}

			scrollHeight = options.parentHeight / options.innerHeight * options.parentHeight;
			innerMoveLength = options.innerHeight - options.parentHeight;
			moveLineLength = options.parentHeight - scrollHeight;

			if ( innerMoveLength > 0 ) {
				if ( !hasScroll ) {
					showScroll();
				}
				$scroll.height(scrollHeight);
			}

			else {
				if ( hasScroll ) {
					hideScroll();
				}
				position = 0;
			}

			onScroll();
		}

		function destroy() {
			$element.off('.scrollAid');
			$scroll.remove();
		}

		createScroll();
		resize(settings);
		$element.on('destroy.scrollAid', destroy);
		$element.on('resize.scrollAid', resize);
		$element.on('scroll.scrollAid', onScroll);
	}

	return $(this).each(function() {
		ScrollAid( $(this), settings );
	});
};