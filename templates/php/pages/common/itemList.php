<div class="b-item-list">
	<div class="b-item-list__el">
		<div class="b-item-list__el__cover">
			<a href="item"><img src="templates/images/_content/items/5.jpg" class="b-item-list__el__cover__img" /></a>
		</div>
		<div class="b-item-list__el__name-and-ratings">
			<div class="b-item-list__el__title"><a href="item" class="b-item-list__el__title__link">Tightropewalker + In Like Satin</a></div>
			<div class="b-item-list__el__artist">Hamid Baroudi</div>
			<div class="b-item-list__el__name-and-ratings__ratings">
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-stat"></span> 4.8
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-plus"></span> 15
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-ok"></span> 332
				</div>
			</div>
		</div>
		<div class="b-item-list__el__info">
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">A</span>QSave Your Heart</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">L</span>Godzilla Rec</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Y</span>2009</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>M</div>
		</div>
		<div class="b-item-list__el__button">
			<a href="cart" class="b-btn is-green is-contains">1 000 руб<div class="b-btn__inner"><i class="icon-download"></i></div></a>
		</div>
	</div>
	<div class="b-item-list__el">
		<div class="b-item-list__el__cover">
			<a href="item"><img src="templates/images/_content/items/1.jpg" class="b-item-list__el__cover__img" /></a>
		</div>
		<div class="b-item-list__el__name-and-ratings">
			<div class="b-item-list__el__title"><a href="item" class="b-item-list__el__title__link">Heart Of Sun / Feel Of Eternity And Even Fufther</a></div>
			<div class="b-item-list__el__artist">Hamid Baroudi</div>
			<div class="b-item-list__el__name-and-ratings__ratings">
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-stat"></span> 4.8
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-plus"></span> 15
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-ok"></span> 332
				</div>
			</div>
		</div>
		<div class="b-item-list__el__info">
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">L</span>Godzilla Rec</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Y</span>2009</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил, MP3, WAV, cD</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>G</div>
		</div>
		<div class="b-item-list__el__button">
			<a href="cart" class="b-btn is-green is-contains">$ 52<div class="b-btn__inner"><i class="icon-download"></i></div></a>
		</div>
	</div>
	<div class="b-item-list__el">
		<div class="b-item-list__el__cover">
			<a href="item"><img src="templates/images/_content/items/4.jpg" class="b-item-list__el__cover__img" /></a>
		</div>
		<div class="b-item-list__el__name-and-ratings">
			<div class="b-item-list__el__title"><a href="item" class="b-item-list__el__title__link">Tightropewalker + In Like Satin</a></div>
			<div class="b-item-list__el__artist">Hamid Baroudi</div>
			<div class="b-item-list__el__name-and-ratings__ratings">
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-stat"></span> 4.8
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-plus"></span> 15
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-ok"></span> 332
				</div>
			</div>
		</div>
		<div class="b-item-list__el__info">
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">A</span>QSave Your Heart</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">L</span>Godzilla Rec</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Y</span>2009</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>M</div>
		</div>
		<div class="b-item-list__el__button">
			<a href="cart" class="b-btn is-green is-contains">$ 158<div class="b-btn__inner"><i class="icon-download"></i></div></a>
		</div>
	</div>
	<div class="b-item-list__el">
		<div class="b-item-list__el__cover">
			<a href="item"><img src="templates/images/_content/items/3.jpg" class="b-item-list__el__cover__img" /></a>
		</div>
		<div class="b-item-list__el__name-and-ratings">
			<div class="b-item-list__el__title"><a href="item" class="b-item-list__el__title__link">Tightropewalker + In Like Satin</a></div>
			<div class="b-item-list__el__artist">Hamid Baroudi</div>
			<div class="b-item-list__el__name-and-ratings__ratings">
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-stat"></span> 4.8
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-plus"></span> 15
				</div>
				<div class="b-item-list__el__name-and-ratings__ratings__rating">
					<span class="icon-ok"></span> 332
				</div>
			</div>
		</div>
		<div class="b-item-list__el__info">
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">A</span>QSave Your Heart</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">L</span>Godzilla Rec</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Y</span>2009</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">T</span>винил</div>
			<div class="b-item-list__el__info__data"><span class="b-item-list__el__info__sym">Q</span>M</div>
		</div>
		<div class="b-item-list__el__button">
			<a href="cart" class="b-btn is-green is-contains">$ 0.99<div class="b-btn__inner"><i class="icon-download"></i></div></a>
		</div>
	</div>
</div>