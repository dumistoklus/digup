<div class="b-content is-market-open" data-module-name="Market" data-url="market">
	<form action="" method="get" class="b-form">
		<div class="b-market">
			<div class="b-market__menu">
				<div class="b-market__menu__links">
					<a href="collection" class="b-market__menu__link is-collection"><i class="icon-collection"></i> Коллекция</a>
					<a href="market" class="b-market__menu__link is-market"><i class="icon-market"></i> Маркет</a>
				</div>
				<div class="b-market__menu__form">
					<div class="b-market__menu__form__layer">
						<div class="b-form__title">
							Сделка
						</div>
						<div class="b-form__checkboxes">
							<label><input type="checkbox" name="type[3]">Продажа</label>
							<label><input type="checkbox" name="type[2]">Обмен</label>
						</div>
					</div>
					<div class="b-market__menu__form__layer">
						<div class="b-form__title">
							Цена, $
						</div>
						<div data-range-slider class="b-form__slider">
							<input type="hidden" name="price[from]" value="0" data-slider-from />
							<input type="hidden" name="price[to]" value="10000" data-slider-to />
							<div data-slider data-min="0" data-max="10000"></div>
							<div class="b-form__slider__hint" data-string="От %num% Р до %num% Р" data-slider-hint></div>
						</div>
					</div>
					<div class="b-market__menu__form__layer">
						<div class="b-form__title">
							Тип носителя
						</div>
						<div class="b-form__checkboxes">
							<label><input type="checkbox" name="type[3]">Винил</label>
							<label><input type="checkbox" name="type[2]">CD / DVD</label>
							<label><input type="checkbox" name="type[1]">MP3 / WAV</label>
						</div>
					</div>
					<div class="b-market__menu__form__layer">
						<div class="b-form__title">
							Качество носителя
						</div>
						<div data-step-slider class="b-form__slider">
							<input type="hidden" name="quality" value="1" data-slider-value />
							<div data-slider></div>
							<div>
								<div class="b-form__slider__step">P</div>
								<div class="b-form__slider__step">F</div>
								<div class="b-form__slider__step">G</div>
								<div class="b-form__slider__step">VG</div>
								<div class="b-form__slider__step">NM</div>
							</div>
						</div>
					</div>
					<div class="b-market__menu__form__layer">
						<div class="b-form__title">
							Рейтинг продавца
						</div>
						<div data-step-slider class="b-form__slider">
							<input type="hidden" name="quality" value="3" data-slider-value />
							<div data-slider></div>
							<div>
								<div class="b-form__slider__step">1</div>
								<div class="b-form__slider__step">2</div>
								<div class="b-form__slider__step">3</div>
								<div class="b-form__slider__step">4</div>
								<div class="b-form__slider__step">5</div>
							</div>
						</div>
					</div>
					<? include('common/for_market_and_collection.php'); ?>
				</div>
			</div>
			<div class="b-market__results">
				<div class="b-market__results__view">
					<a href="javascript:void(0)" class="b-market__results__view__link" data-view-as-list><i class="icon-list-as-list"></i></a>
					<a href="javascript:void(0)" class="b-market__results__view__link is-active" data-view-as-cards><i class="icon-list-as-cards"></i></a>
				</div>
				<div class="b-market__results__search-field">
					<input type="search" name="q" placeholder="НАЙТИ">
				</div>
				<div class="b-market__results__items" data-items>
					<? // По данному комментарию отрезается контент. Не удалять, если нужен ajax ?>
					<!-- ITEMS -->
					<?
					$items = array(
						array(
							'name'			=> 'Faces',
							'hint'			=> 'Grave Motion',
							'price_from'	=> '35',
							'image'			=> 'templates/images/_content/items/2.jpg'
						),
						array(
							'name'			=> 'Off The Deep End',
							'hint'			=> 'Weird All Yankivic',
							'price_from'	=> '19',
							'image'			=> 'templates/images/_content/items/4.jpg'
						),
						array(
							'name'			=> 'Classic Gangster',
							'hint'			=> 'Jay-Z',
							'price_from'	=> '40',
							'image'			=> 'templates/images/_content/items/3.jpg'
						),
						array(
							'name'			=> 'Under The Iron Sea',
							'hint'			=> 'Keane',
							'price_from'	=> '40',
							'image'			=> 'templates/images/_content/items/1.jpg'
						),
						array(
							'name'			=> 'Funky Town',
							'hint'			=> 'Prince',
							'price_from'	=> '40',
							'image'			=> 'templates/images/_content/items/7.jpg'
						),
						array(
							'name'			=> 'Funky Town',
							'hint'			=> 'Prince',
							'price_from'	=> '40',
							'image'			=> 'templates/images/_content/items/6.jpg'
						),
						array(
							'name'			=> 'Funky Town',
							'hint'			=> 'Prince',
							'price_from'	=> '40',
							'image'			=> 'templates/images/_content/items/5.jpg'
						)
					);
					$i = mt_rand(19, 20);
					while ( $i-- ) {

						$random = mt_rand(0, count( $items ) - 1 );
						$item	= $items[$random];
						?>

					<a href="item" class="b-market__results__item b-item" data-item-id="<?= mt_rand(0, 100000)?>">
						<div class="b-item__img" style="background-image: url(<?= $item['image'] ?>)"></div>
						<div class="b-item__text">
							<div class="b-item__text__name"><?= $item['name'] ?></div>
							<div class="b-item__text__hint"><?= $item['hint'] ?></div>
						</div>
						<div class="b-item__overlay">
							<div class="b-item__overlay__button-wrapper">
								<span class="b-item__button is-contains" data-add-to-cart>от <?= $item['price_from'] ?> $<div class="b-btn__inner"><i class="icon-download"></i></div></span>
							</div>
							<div class="b-item__overlay__data"><span class="b-item__overlay__sym">A</span>QSave Your Heart</div>
							<div class="b-item__overlay__data"><span class="b-item__overlay__sym">L</span>Godzilla Rec</div>
							<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Y</span>2009</div>
							<div class="b-item__overlay__data"><span class="b-item__overlay__sym">T</span>винил</div>
							<div class="b-item__overlay__data"><span class="b-item__overlay__sym">Q</span>M</div>
						</div>
					</a>
					<? } ?>
					<? // По данному комментарию отрезается контент. Не удалять, если нужен ajax ?>
					<!-- / ITEMS -->
				</div>
				<a href="javascript:void(0)" class="b-more-link" data-more>Показать больше <i class="icon-arrow-down"></i></a>
				<div class="b-loader"></div>
			</div>
		</div>
	</form>
</div>